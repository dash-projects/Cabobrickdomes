<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Homepage
Route::get('/', ['as'=>'home','uses'=>'HomeController@index']);
Route::get('/galeria', ['as'=>'gallery','uses'=>'HomeController@galery']);
Route::get('/trabajos', ['as'=>'works','uses'=>'HomeController@works']);
Route::get('/materiales', ['as' => 'materials', 'uses' => 'HomeController@materials']);
Route::get('/nosotros', ['as' => 'about', 'uses' => 'HomeController@about']);
//Rutas en Inglés
Route::get('/en', ['as' => 'home-en', 'uses' => 'HomeController@indexEn']);
Route::get('/gallery', ['as' => 'gallery-en', 'uses' => 'HomeController@galeryEn']);
Route::get('/works', ['as' => 'works-en', 'uses' => 'HomeController@worksEn']);
Route::get('/materials', ['as' => 'materials-en', 'uses' => 'HomeController@materialsEn']);
Route::get('/about-us', ['as' => 'about-en', 'uses' => 'HomeController@aboutEn']);
