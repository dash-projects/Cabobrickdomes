<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexEn()
    {
        App::setLocale('en');
        return $this->index();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function galery()
    {
        $sliders = [
            ['title'=>trans('header.menu-3-1'),'id'=>trans('header.menu-3-1-idid'),'images'=>[
                ['url' => '/img/gallery/hornos/horno-01.jpg', 'title' => 'Horno'],
                ['url' => '/img/gallery/hornos/horno-02.jpg', 'title' => 'Horno'],
                ['url' => '/img/gallery/hornos/horno-03.jpg', 'title' => 'Horno'],
                ['url' => '/img/gallery/hornos/horno-04.jpg', 'title' => 'Horno'],
                ['url' => '/img/gallery/hornos/horno-05.jpg', 'title' => 'Horno'],
                ['url' => '/img/gallery/hornos/horno-06.jpg', 'title' => 'Horno'],
                ['url' => '/img/gallery/hornos/horno-07.jpg', 'title' => 'Horno'],
                ['url' => '/img/gallery/hornos/horno-08.jpg', 'title' => 'Horno'],
                ['url' => '/img/gallery/hornos/horno-09.jpg', 'title' => 'Horno'],
                ['url' => '/img/gallery/hornos/horno-10.jpg', 'title' => 'Horno'],
                ['url' => '/img/gallery/hornos/horno-11.jpg', 'title' => 'Horno'],
                ['url' => '/img/gallery/hornos/horno-12.jpg', 'title' => 'Horno'],
                ['url' => '/img/gallery/hornos/horno-13.jpg', 'title' => 'Horno'],
                ['url' => '/img/gallery/hornos/horno-14.jpg', 'title' => 'Horno'],
                ['url' => '/img/gallery/hornos/horno-15.jpg', 'title' => 'Horno'],
                ['url' => '/img/gallery/hornos/horno-16.png', 'title' => 'Horno']
                ]
            ],
            ['title' => trans('header.menu-3-2'), 'id' => trans('header.menu-3-2-id'), 'images' => [
                ['url' => '/img/gallery/cupulas_circulares/cupula-01.jpg', 'title' => 'Cupula Circular'],
                ['url' => '/img/gallery/cupulas_circulares/cupula-02.jpg', 'title' => 'Cupula Circular'],
                ['url' => '/img/gallery/cupulas_circulares/cupula-03.jpg', 'title' => 'Cupula Circular'],
                ['url' => '/img/gallery/cupulas_circulares/cupula-04.jpg', 'title' => 'Cupula Circular'],
                ['url' => '/img/gallery/cupulas_circulares/cupula-05.jpg', 'title' => 'Cupula Circular'],
                ['url' => '/img/gallery/cupulas_circulares/cupula-06.jpg', 'title' => 'Cupula Circular'],
                ['url' => '/img/gallery/cupulas_circulares/cupula-07.jpg', 'title' => 'Cupula Circular'],
                ['url' => '/img/gallery/cupulas_circulares/cupula-08.jpg', 'title' => 'Cupula Circular'],
                ['url' => '/img/gallery/cupulas_circulares/cupula-09.jpg', 'title' => 'Cupula Circular'],
                ['url' => '/img/gallery/cupulas_circulares/cupula-10.jpg', 'title' => 'Cupula Circular'],
                ['url' => '/img/gallery/cupulas_circulares/cupula-11.jpg', 'title' => 'Cupula Circular'],
                ['url' => '/img/gallery/cupulas_circulares/cupula-12.jpg', 'title' => 'Cupula Circular'],
                ['url' => '/img/gallery/cupulas_circulares/cupula-13.jpg', 'title' => 'Cupula Circular'],
                ['url' => '/img/gallery/cupulas_circulares/cupula-14.jpg', 'title' => 'Cupula Circular'],
                ['url' => '/img/gallery/cupulas_circulares/cupula-15.jpg', 'title' => 'Cupula Circular'],
                ['url' => '/img/gallery/cupulas_circulares/cupula-16.jpg', 'title' => 'Cupula Circular'],
                ['url' => '/img/gallery/cupulas_circulares/cupula-17.jpg', 'title' => 'Cupula Circular'],
                ['url' => '/img/gallery/cupulas_circulares/cupula-18.jpg', 'title' => 'Cupula Circular'],
                ['url' => '/img/gallery/cupulas_circulares/cupula-19.jpg', 'title' => 'Cupula Circular'],
                ['url' => '/img/gallery/cupulas_circulares/cupula-20.jpg', 'title' => 'Cupula Circular'],
                ['url' => '/img/gallery/cupulas_circulares/cupula-21.jpg', 'title' => 'Cupula Circular'],
                ['url' => '/img/gallery/cupulas_circulares/cupula-22.jpg', 'title' => 'Cupula Circular'],
                ['url' => '/img/gallery/cupulas_circulares/cupula-23.jpg', 'title' => 'Cupula Circular'],
                ['url' => '/img/gallery/cupulas_circulares/cupula-24.jpg', 'title' => 'Cupula Circular'],
                ['url' => '/img/gallery/cupulas_circulares/cupula-25.jpg', 'title' => 'Cupula Circular'],
                ['url' => '/img/gallery/cupulas_circulares/cupula-26.jpg', 'title' => 'Cupula Circular'],
                ['url' => '/img/gallery/cupulas_circulares/cupula-27.jpg', 'title' => 'Cupula Circular'],
                ['url' => '/img/gallery/cupulas_circulares/cupula-28.jpg', 'title' => 'Cupula Circular'],
                ['url' => '/img/gallery/cupulas_circulares/cupula-29.jpg', 'title' => 'Cupula Circular'],
                //cupulas cuadradas
                ['url' => '/img/gallery/cupulas_cuadradas/cupula-01.jpg', 'title' => 'Cupula Cuadrada'],
                ['url' => '/img/gallery/cupulas_cuadradas/cupula-02.jpg', 'title' => 'Cupula Cuadrada'],
                ['url' => '/img/gallery/cupulas_cuadradas/cupula-03.jpg', 'title' => 'Cupula Cuadrada'],
                ['url' => '/img/gallery/cupulas_cuadradas/cupula-04.jpg', 'title' => 'Cupula Cuadrada'],
                ['url' => '/img/gallery/cupulas_cuadradas/cupula-05.jpg', 'title' => 'Cupula Cuadrada'],
                ['url' => '/img/gallery/cupulas_cuadradas/cupula-06.jpg', 'title' => 'Cupula Cuadrada'],
                ['url' => '/img/gallery/cupulas_cuadradas/cupula-07.jpg', 'title' => 'Cupula Cuadrada'],
                ['url' => '/img/gallery/cupulas_cuadradas/cupula-08.jpg', 'title' => 'Cupula Cuadrada'],
                ['url' => '/img/gallery/cupulas_cuadradas/cupula-09.jpg', 'title' => 'Cupula Cuadrada'],
                ['url' => '/img/gallery/cupulas_cuadradas/cupula-10.jpg', 'title' => 'Cupula Cuadrada'],
                ['url' => '/img/gallery/cupulas_cuadradas/cupula-11.jpg', 'title' => 'Cupula Cuadrada'],
                ['url' => '/img/gallery/cupulas_cuadradas/cupula-12.jpg', 'title' => 'Cupula Cuadrada'],
                ['url' => '/img/gallery/cupulas_cuadradas/cupula-13.jpg', 'title' => 'Cupula Cuadrada'],
                ['url' => '/img/gallery/cupulas_cuadradas/cupula-14.jpg', 'title' => 'Cupula Cuadrada'],
                ['url' => '/img/gallery/cupulas_cuadradas/cupula-15.jpg', 'title' => 'Cupula Cuadrada'],
                ['url' => '/img/gallery/cupulas_cuadradas/cupula-16.jpg', 'title' => 'Cupula Cuadrada'],
                ['url' => '/img/gallery/cupulas_cuadradas/cupula-17.jpg', 'title' => 'Cupula Cuadrada'],
                ['url' => '/img/gallery/cupulas_cuadradas/cupula-21.jpg', 'title' => 'Cupula Cuadrada'],
                ['url' => '/img/gallery/cupulas_cuadradas/cupula-23.jpg', 'title' => 'Cupula Cuadrada'],
                ['url' => '/img/gallery/cupulas_cuadradas/cupula-24.jpg', 'title' => 'Cupula Cuadrada'],
                ['url' => '/img/gallery/cupulas_cuadradas/cupula-25.jpg', 'title' => 'Cupula Cuadrada'],
                ['url' => '/img/gallery/cupulas_cuadradas/cupula-26.png', 'title' => 'Cupula Cuadrada'],
                ['url' => '/img/gallery/cupulas_cuadradas/cupula-27.png', 'title' => 'Cupula Cuadrada'],
                ['url' => '/img/gallery/cupulas_cuadradas/cupula-28.png', 'title' => 'Cupula Cuadrada'],
                ['url' => '/img/gallery/cupulas_cuadradas/cupula-29.jpg', 'title' => 'Cupula Cuadrada'],
                // Cupulas Ovaladas
                ['url' => '/img/gallery/cupulas_ovaladas/cupula-01.jpg', 'title' => 'Cupula Ovalada'],
                ['url' => '/img/gallery/cupulas_ovaladas/cupula-02.jpg', 'title' => 'Cupula Ovalada'],
                ['url' => '/img/gallery/cupulas_ovaladas/cupula-03.jpg', 'title' => 'Cupula Ovalada'],
                ['url' => '/img/gallery/cupulas_ovaladas/cupula-05.jpg', 'title' => 'Cupula Ovalada'],
                ['url' => '/img/gallery/cupulas_ovaladas/cupula-06.jpg', 'title' => 'Cupula Ovalada'],
                //Bovedas de Canñon

                ['url' => '/img/gallery/bovedas_canon/canon-01.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-02.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-03.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-04.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-05.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-06.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-07.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-08.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-09.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-10.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-11.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-12.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-13.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-14.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-15.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-16.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-17.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-18.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-19.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-20.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-21.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-22.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-23.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-24.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-25.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-26.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-27.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-28.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-29.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-30.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-31.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-32.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-33.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-34.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-36.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-37.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-38.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-39.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-40.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-41.jpg', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-42.png', 'title' => 'Bóveda de cañon'],
                ['url' => '/img/gallery/bovedas_canon/canon-43.png', 'title' => 'Bóveda de cañon'],
            ]], 
            ['title' => trans('header.menu-3-3'), 'id' => trans('header.menu-3-3-id'), 'images' => [
                ['url' => '/img/gallery/chimeneas/chimenea-01.jpg', 'title' => 'Chimenea'],
                ['url' => '/img/gallery/chimeneas/chimenea-02.jpg', 'title' => 'Chimenea'],
                ['url' => '/img/gallery/chimeneas/chimenea-03.jpg', 'title' => 'Chimenea'],
                ['url' => '/img/gallery/chimeneas/chimenea-04.jpg', 'title' => 'Chimenea'],
                ['url' => '/img/gallery/chimeneas/chimenea-05.jpg', 'title' => 'Chimenea'],
                ['url' => '/img/gallery/chimeneas/chimenea-06.jpg', 'title' => 'Chimenea'],
                ['url' => '/img/gallery/chimeneas/chimenea-07.jpg', 'title' => 'Chimenea'],
                ['url' => '/img/gallery/chimeneas/chimenea-08.jpg', 'title' => 'Chimenea'],
                ['url' => '/img/gallery/chimeneas/chimenea-09.jpg', 'title' => 'Chimenea'],
                ['url' => '/img/gallery/chimeneas/chimenea-10.jpg', 'title' => 'Chimenea'],
                ['url' => '/img/gallery/chimeneas/chimenea-11.jpg', 'title' => 'Chimenea'],
                ['url' => '/img/gallery/chimeneas/chimenea-12.jpg', 'title' => 'Chimenea'],
                ['url' => '/img/gallery/chimeneas/chimenea-13.jpg', 'title' => 'Chimenea'],
                ['url' => '/img/gallery/chimeneas/chimenea-14.jpg', 'title' => 'Chimenea'],
                ['url' => '/img/gallery/chimeneas/chimenea-15.jpg', 'title' => 'Chimenea'],
                ['url' => '/img/gallery/chimeneas/chimenea-16.jpg', 'title' => 'Chimenea'],
                ['url' => '/img/gallery/chimeneas/chimenea-17.jpg', 'title' => 'Chimenea'],
                ['url' => '/img/gallery/chimeneas/chimenea-18.jpg', 'title' => 'Chimenea'],
                ['url' => '/img/gallery/chimeneas/chimenea-19.jpg', 'title' => 'Chimenea'],
                ['url' => '/img/gallery/chimeneas/chimenea-20.jpg', 'title' => 'Chimenea'],
                ['url' => '/img/gallery/chimeneas/chimenea-21.jpg', 'title' => 'Chimenea'],
                ['url' => '/img/gallery/chimeneas/chimenea-22.jpg', 'title' => 'Chimenea'],
                ['url' => '/img/gallery/chimeneas/chimenea-23.jpg', 'title' => 'Chimenea'],
                ['url' => '/img/gallery/chimeneas/chimenea-24.jpg', 'title' => 'Chimenea'],
                ['url' => '/img/gallery/chimeneas/chimenea-25.jpg', 'title' => 'Chimenea'],
                ['url' => '/img/gallery/chimeneas/chimenea-26.jpg', 'title' => 'Chimenea'],
                ['url' => '/img/gallery/chimeneas/chimenea-27.jpg', 'title' => 'Chimenea'],
                ['url' => '/img/gallery/chimeneas/chimenea-28.png', 'title' => 'Chimenea'],
                ['url' => '/img/gallery/chimeneas/chimenea-29.png', 'title' => 'Chimenea'],
                ['url' => '/img/gallery/chimeneas/chimenea-30.png', 'title' => 'Chimenea'],
                ['url' => '/img/gallery/chimeneas/chimenea-31.png', 'title' => 'Chimenea'],
                ['url' => '/img/gallery/chimeneas/chimenea-32.png', 'title' => 'Chimenea'],
                ['url' => '/img/gallery/chimeneas/chimenea-33.png', 'title' => 'Chimenea'],
                ]
            ],
            ['title' => trans('header.menu-3-5'), 'id' => trans('header.menu-3-5-id'), 'images' => [
                ['url' => '/img/gallery/asadores/asador-01.jpg', 'title' => 'Asador'],
                ['url' => '/img/gallery/asadores/asador-02.jpg', 'title' => 'Asador'],
                ['url' => '/img/gallery/asadores/asador-03.jpg', 'title' => 'Asador'],
                ['url' => '/img/gallery/asadores/asador-04.jpg', 'title' => 'Asador'],
                ['url' => '/img/gallery/asadores/asador-05.jpg', 'title' => 'Asador'],
                ['url' => '/img/gallery/asadores/asador-06.jpg', 'title' => 'Asador'],
                ['url' => '/img/gallery/asadores/asador-07.jpg', 'title' => 'Asador'],
                ['url' => '/img/gallery/asadores/asador-08.jpg', 'title' => 'Asador'],
                ['url' => '/img/gallery/asadores/asador-09.png', 'title' => 'Asador'],
            ]],
            ['title' => trans('header.menu-3-4'), 'id' => trans('header.menu-3-4-id'), 'images' => [
                ['url' => '/img/gallery/trabajos_ladrillo/fachada-01.jpg', 'title' => 'Asador'],
                ['url' => '/img/gallery/trabajos_ladrillo/fachada-02.jpg', 'title' => 'Asador'],
                ['url' => '/img/gallery/trabajos_ladrillo/fachada-03.jpg', 'title' => 'Asador'],
                ['url' => '/img/gallery/trabajos_ladrillo/fachada-04.jpg', 'title' => 'Asador'],
                ['url' => '/img/gallery/trabajos_ladrillo/fachada-05.jpg', 'title' => 'Asador'],
                ['url' => '/img/gallery/trabajos_ladrillo/fachada-06.jpg', 'title' => 'Asador'],
                ['url' => '/img/gallery/trabajos_ladrillo/fachada-07.jpg', 'title' => 'Asador'],
                ['url' => '/img/gallery/trabajos_ladrillo/fachada-08.jpg', 'title' => 'Asador'],
                ['url' => '/img/gallery/trabajos_ladrillo/fachada-09.png', 'title' => 'Asador'],
                ['url' => '/img/gallery/trabajos_ladrillo/fachada-10.png', 'title' => 'Asador'],
                ['url' => '/img/gallery/trabajos_ladrillo/fachada-11.jpg', 'title' => 'Asador']
            ]]

        ];
        return view('gallery',['sliders'=>$sliders]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function galeryEn()
    {
        App::setLocale('en');
        return $this->galery();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function works()
    {
        return view('works');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function worksEn()
    {
        App::setLocale('en');
        return $this->works();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function about()
    {
        return view('about');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function aboutEn()
    {
        App::setLocale('en');
        return $this->about();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function materials()
    {
        return view('materials');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function materialsEn()
    {
        App::setLocale('en');
        return $this->materials();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function sendContact(Request $request)
    {
      return $request->all();
        // return '<pre>'.print_r($variables,true).'</pre>';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
