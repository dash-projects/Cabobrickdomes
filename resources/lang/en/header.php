<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Header Language Lines
    |--------------------------------------------------------------------------
    |
    | Las siguientes lineas son traducciones del sitio, forman parte del diccionario
    | que proporciona las interpretaciones multi-idioma.
    |
 */
    'days'=> 'Mon - Sat',
    'option-lang' => 'Language / Idioma:',
    'home-url' => route('home-en'),
    'menu' => 'Menu',
    'menu-1' => 'Home',
    'menu-1-1' => 'Our Work\'s Types',
    'menu-1-1-url' => route('home') . '#works-types',
    'menu-2' => 'Gallery',
    'menu-2-1' => 'Ovens',
    'menu-2-2' => 'Domes',
    'menu-2-3' => 'Fireplaces',
    'menu-2-4' => 'Fronts',
    'menu-2-5' => 'Grills',
    'menu-2-6' => 'Wine Cellar',
    'menu-2-1-url' => route('gallery-en') . '#ovens',
    'menu-2-2-url' => route('gallery-en') . '#domes',
    'menu-2-3-url' => route('gallery-en') . '#fireplaces',
    'menu-2-4-url' => route('gallery-en') . '#facades',
    'menu-2-5-url' => route('gallery-en') . '#grills',
    'menu-2-6-url' => route('gallery-en') . '#wine-cellars',
    'menu-3' => 'Works',
    'menu-3-1' => 'Ovens',
    'menu-3-2' => 'Domes',
    'menu-3-3' => 'Fireplaces',
    'menu-3-4' => 'Fronts',
    'menu-3-5' => 'Grills',
    'menu-3-6' => 'Wine Cellar',
    'menu-3-1-id' => '#ovens',
    'menu-3-2-id' => '#domes',
    'menu-3-3-id' => '#fireplaces',
    'menu-3-4-id' => '#facades',
    'menu-3-5-id' => '#grills',
    'menu-3-1-url' => route('works-en').'#ovens',
    'menu-3-2-url' => route('works-en').'#domes',
    'menu-3-3-url' => route('works-en').'#fireplaces',
    'menu-3-4-url' => route('works-en').'#facades',
    'menu-3-5-url' => route('works-en').'#grills',
    'menu-3-6-url' => route('works-en').'#wine-cellars',
    'menu-4' => 'About',
    'menu-4-1' => 'About us',
    'menu-4-2' => 'Contact us',
    'menu-4-3' => 'More about us',
    'menu-4-4' => 'Our Achievements',
    'menu-4-1-url' => route('about-en').'#about-us',
    'menu-4-2-url' => route('about-en').'#contact-us',
    'menu-4-3-url' => route('about-en').'#learn-more',
    'menu-4-4-url' => route('about-en').'#our-achievements',
    'menu-5' => 'Construction materials',
    'menu-5-1' => 'Bricks',
    'menu-5-2' => 'Lattices',
    'menu-5-3' => 'Roof tiles',
    'menu-5-4' => 'Floors',
    'menu-5-5' => 'Facades',
    'menu-5-1-url' => route('materials-en') . '#bricks',
    'menu-5-2-url' => route('materials-en') . '#lattices',
    'menu-5-3-url' => route('materials-en') . '#roofs',
    'menu-5-4-url' => route('materials-en') . '#floors',
    'menu-5-5-url' => route('materials-en') . '#facades',
    'menu-6' => 'Contact',
    'menu-7' => 'Language',
    'search-ph' => 'Type and hit Intro',

];