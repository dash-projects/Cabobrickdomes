<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Footer Language Lines
    |--------------------------------------------------------------------------
    |
    | Las siguientes lineas son traducciones del sitio, forman parte del diccionario
    | que proporciona las interpretaciones multi-idioma.
    |
 */

    'title-1' => 'About Us',
    'title-2' => 'Quick links',
    'title-3' => 'Contact Us',
    'quick-link-1' => 'Ovens',
    'quick-link-2' => 'Domes',
    'quick-link-3' => 'Fireplaces',
    'quick-link-4' => 'Fronts',
    'quick-link-5' => 'Grills',
    'quick-link-6' => 'Wine Cellers',
    'contact-p' => 'If you have questions or want to request a quote, contact us by the following ways:',
    'copy-right' => 'CaboBrickDomes 2018 Copyright &copy;. All rights reserved. Website Powered by',

];