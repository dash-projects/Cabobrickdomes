<?php

use Illuminate\Queue\Console\WorkCommand;

return [

    /*
    |--------------------------------------------------------------------------
    | Content Language Lines
    |--------------------------------------------------------------------------
    |
    | Las siguientes lineas son traducciones del sitio, forman parte del diccionario
    | que proporciona las interpretaciones multi-idioma.
    |
 */
    //SLIDER
    'slider1-title'=> 'WELCOME TO <strong>CABO BRICK DOMES</strong>',
    'slider2-title' => 'We know better than anyone the <strong>art of Brick</strong>',
    'slider3-title' => 'Get ready for the next winter',
    'slider4-title' => 'The best wines are only in the best <strong>Cellars</strong>',
    'slider5-title' => 'A traditional <strong>facade</strong> for your house',
    'slider6-title' => 'Let\'s roast meat',
    'slider1-subtitle1' => 'Do you need a brick oven?',
    'slider2-subtitle1' => 'The best domes in Mexico are made by us',
    'slider3-subtitle1' => 'With a beautiful fireplace in your home',
    'slider4-subtitle1' => 'Customize your property ',
    'slider5-subtitle1' => 'Only with Cabo Brick Domes',
    'slider6-subtitle1' => 'Let Cabo Brick Domes do it for you',
    'slider1-subtitle2' => 'We do it as you want',
    'slider2-subtitle2' => '',
    'slider3-subtitle2' => '',
    'slider4-subtitle2' => 'Only with Cabo Brick Domes',
    'slider5-subtitle2' => '',
    'slider6-subtitle2' => 'Get a Grill for you.',
    'slider1-btn' => 'See more',
    'slider2-btn' => 'I want it!',
    'slider3-btn' => 'I\'m interested!',
    'slider4-btn' => 'I\'m interested!',
    'slider5-btn' => 'I\'m interested!',
    'slider6-btn' => 'Let\s do it!',
    'slider1-url' => route('works-en') . '#ovens',
    'slider2-url' => route('works-en') . '#domes',
    'slider3-url' => route('works-en') . '#fireplaces',
    'slider4-url' => route('works-en') . '#wine-cellars',
    'slider5-url' => route('works-en') . '#facades',
    'slider6-url' => route('works-en') . '#grills',
    //WORK
    'work-type-title' => 'Work Types',
    'work-type-id' => 'work-types',
    'work-type-desc' => 'WE PRESENT YOU A LIST OF THE TYPES OF WORK WE CARRY OUT',
    'work-1' => 'Ovens',
    'work-2' => 'Domes',
    'work-3' => 'Fireplaces',
    'work-4' => 'Facades',
    'work-5' => 'Grills',
    'work-6' => 'Wine Cellars', 
    //MATERIALS
    'material-desc' => 'IN THIS SECTION YOU WILL FIND :MAT OF DIFFERENT DIMENSIONS AND CHARACTERISTICS, SO THAT YOU CAN CHOOSE WHICH MOST IS ACCORDING TO YOUR NEEDS',
    'material-title' => ':Mat',
    'material-id' => ':mat',
    'material-brick' => 'brick|bricks',
    'material-lattice' => 'lattice|lattices',
    'material-roof' => 'roof|roofs',
    'material-floor' => 'floor|floors',
    'material-facade' => 'facade|facades',
    'material-dimension' => 'Dimensions:',
    'material-quantity' => 'Quantity per m<sup>2</sup>: :quantity Pieces',
    'material-code' => 'Code:',
    'material-brick-1' => 'Dark brick',
    'material-brick-2' => 'Natural terracotta brick',
    'material-brick-3' => 'Sand colored brick',
    'material-brick-4' => 'Brick for rustic wall',
    'material-brick-5' => 'Brick for apparent wall',
    'material-brick-6' => 'Machined brick',
    'material-brick-7' => 'Pichuela Brick',
    'material-brick-8' => 'Annealed brick',
    'material-brick-9' => 'Firebrick',
    'material-brick-10' => 'Pressed brick',
    'material-lattice-1' => 'Clay latticework',
    'material-lattice-2' => 'Clay latticework',
    'material-lattice-3' => 'Clay latticework',
    'material-lattice-4' => 'Clay latticework',
    'material-lattice-5' => 'Clay latticework',
    'material-lattice-6' => 'Clay latticework',
    'material-lattice-7' => 'Clay latticework',
    'material-roof-1' => 'Natural treasure tile',
    'material-roof-2' => 'Natural treasure tile',
    'material-roof-3' => 'Punta Ballena ranchera Tile',
    'material-roof-4' => 'Greek wing tile',
    'material-floor-1' => 'Terracotta fine floor',
    'material-floor-2' => 'Terracotta smooth plain natural',
    'material-floor-3' => 'Terracotta rustic floor',
    'material-floor-4' => 'Flat fine terracotta floor',
    'material-floor-5' => 'Floor peron',
    'material-floor-6' => 'Terracotta fine floor',
    'material-floor-7' => 'Terracotta fine floor',
    'material-facade-1' => 'Pichuela Facade',
    'material-facade-2' => 'Regular Facade',
    'material-facade-3' => 'Facade beveled refractory',
    'material-facade-4' => 'volcano Facade',
    'material-facade-5' => 'Red Facade',
    'material-facade-6' => 'Abano Facade',
    'material-facade-7' => 'Natural Facade',
    'material-facade-8' => 'Dark Facade',
    'material-facade-9' => 'Sand Facade',
    //Works
    'work-filter-1'=>'ALL OUR WORKS',
    'work-filter-2'=>'OVENS',
    'work-filter-3'=>'CUPLES',
    'work-filter-4'=>'FIREPLACES',
    'work-filter-5'=>'FACADES',
    'work-filter-6'=>'GRILLS',
    'work-filter-7'=>'WINE CELLARS',
    'work-id-ovens'=>'ovens',
    'work-id-domes'=>'cupolas',
    'work-id-fireplaces'=>'fireplaces',
    'work-id-facades'=>'facades',
    'work-id-grill'=>'grills',
    'work-id-cellars'=>'cellars',
    'work-title-product-1'=>'Oven with Brick Pichuela',
    'work-title-product-2'=>'Oval Dome',
    'work-title-product-3'=>'Fireplace',
    'work-title-product-4'=>'Facade',
    'work-title-product-5'=>'Grill',
    'work-title-product-6'=>'Wine Cellar',
    'work-title-product-7'=>'Oval Dome',
    'work-title-product-8'=>'Oven made with annealed brick',
    'work-title-product-9'=>'Circular dummy',
    'work-title-product-10'=>'Circular dummy',
    'work-title-product-11'=>'Barrel vault',
    'work-title-product-12'=>'Temazcal',
    'work-desc-product-1'=>'Oven and base made with brick punch for residential house, dimensions 1.20M interior',
    'work-desc-product-2'=>'Elliptical vault with reliefs to the center made with brick rustic pichuela',
    'work-desc-product-3'=>'Fireplace corner measured 0.85 x 2.70 M high, polished and tinted with acid',
    'work-desc-product-4'=>'Brick facade with arches and molding dimensions 8 x 3.50M with Guadalajara brick',
    'work-desc-product-5'=>'Roaster, arches, oven and base to store firewood 5M long',
    'work-desc-product-6'=>'Wine Cellar, arched dome with arches, 8M inner diameter and lantern',
    'work-desc-product-7'=>'Oval dome with design dimensions 10x4 M with rustic brick',
    'work-desc-product-8'=>'Oven with base and bar for food preparation made with annealed brick designed for residential houses',
    'work-desc-product-9'=>'Circular dome 3 m in diameter with 5-star star design with pigeon breast molding',
    'work-desc-product-10'=>'Dome in main bedroom of 6 M in diameter with lantern with design of aids',
    'work-desc-product-11'=>'Barrel vault made with annealed brick in main entrance with design',
    'work-desc-product-12'=>'Oven made with special refractory material to prepare pizzas to the firewood',
    'work-btn' => 'Make a quote',
    //ABOUT
    'about-title-1' =>'About us',
    'about-desc-1'  =>'In this section you will find, information about Us, such as the origin, our mission, vision and Values.',
    'about-subtitle-1'  =>'About us?',
    'about-subdesc-1'   =>'We are a family company founded in 1999 that has been dedicated and specialized in building the best brick domes. We have known brickwork for more than 20 years, We are the expert you need.',
    'about-mision'  =>'Mission',
    'about-mision-desc' =>'""Build the best domes, furnaces and brick chimneys in all of Mexico."',
    'about-vision'  =>'View',
    'about-vision-desc' =>'""To be the best builders of domes, kilns and chimneys of all Mexico."',
    'about-values'  =>'Values',
    'about-values-desc' =>'""Honesty, Commitment, Trust, Professionalism and Punctuality"',
    'about-contact-desc' => 'If you have questions or want to request a quote, without commitment, contact us, We are the experts you need',
    'about-btn-call' => 'CALL',
    'about-btn-quote' =>'QUICK QUOTE',
    'about-section-id-1'=>'about-us',
    'about-section-id-2'=>'contact-us',
    'about-section-id-3'=>'learn-more',
    'about-section-id-4'=>'our-achievements',
    'about-learn-more-title'=>'Learn more about us',
    'about-learn-more-subtitle'=>'WHY CHOOSE US?',
    'about-learn-more-desc'=>'We give you some reasons why you should choose us',
    'about-learn-more-1'=>'20+ Years of Experience',
    'about-learn-more-1-desc'=>'Since before our foundation, we have worked with an objective in sight, to deliver an exceptional, well-built work',
    'about-learn-more-2'=>'EXPERTS ON THE BRICK',
    'about-learn-more-2-desc'=>'For brickwork and finishing, we are experts with hundreds of satisfied customers who support us.',
    'about-learn-more-3'=>'THE BEST CUSTOMER ATTENTION',
    'about-learn-more-3-desc'=>'We strive to continue offering a very good treatment and attention that the client needs.',
    'about-achiev-title'=>'Our Achievement',
    'about-achiev-desc'=>'Here is a brief summary of our achievements',
    'about-achiev-1'=>'Completed Projects',
    'about-achiev-2'=>'Domes',
    'about-achiev-3'=>'Satisfied customers',
    'general' => ':en',
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    


];