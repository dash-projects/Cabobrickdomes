<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Header Language Lines
    |--------------------------------------------------------------------------
    |
    | Las siguientes lineas son traducciones del sitio, forman parte del diccionario
    | que proporciona las interpretaciones multi-idioma.
    |
 */
    'days'=> 'Lun - Sáb',
    'option-lang' => 'Idioma / Language:',
    'home-url'=> route('home'),
    'menu' => 'Menú',
    'menu-1' => 'Inicio',
    'menu-1-1' => 'Tipos de Trabajos',
    'menu-1-1-url' => route('home') . '#tipos-de-trabajos',
    'menu-2' => 'Galería',
    'menu-2-1' => 'Hornos',
    'menu-2-2' => 'Cúpulas',
    'menu-2-3' => 'Chimeneas',
    'menu-2-4' => 'Fachadas',
    'menu-2-5' => 'Asadores',
    'menu-2-6' => 'Cavas',
    'menu-2-1-url' => route('gallery') . '#hornos',
    'menu-2-2-url' => route('gallery') . '#cupulas',
    'menu-2-3-url' => route('gallery') . '#chimeneas',
    'menu-2-4-url' => route('gallery') . '#fachadas',
    'menu-2-5-url' => route('gallery') . '#asadores',
    'menu-2-6-url' => route('gallery') . '#cavas',
    'menu-3' => 'Trabajos',
    'menu-3-1' => 'Hornos',
    'menu-3-2' => 'Cúpulas',
    'menu-3-3' => 'Chimeneas',
    'menu-3-4' => 'Fachadas',
    'menu-3-5' => 'Asadores',
    'menu-3-6' => 'Cavas',
    'menu-3-1-id' => 'hornos',
    'menu-3-2-id' => 'cupulas',
    'menu-3-3-id' => 'chimeneas',
    'menu-3-4-id' => 'fachadas',
    'menu-3-5-id' => 'asadores',
    'menu-3-1-url' => route('works').'#hornos',
    'menu-3-2-url' => route('works').'#cupulas',
    'menu-3-3-url' => route('works').'#chimeneas',
    'menu-3-4-url' => route('works').'#fachadas',
    'menu-3-5-url' => route('works').'#asadores',
    'menu-3-6-url' => route('works').'#cavas',
    'menu-4' => 'Nosotros',
    'menu-4-1' => 'Acerca de Nosotros',
    'menu-4-2' => 'Contáctanos',
    'menu-4-3' => 'Conónoces Más',
    'menu-4-4' => 'Nuestros Logros',
    'menu-4-1-url' => route('about').'#acerca-de-nosotros',
    'menu-4-2-url' => route('about').'#contactanos',
    'menu-4-3-url' => route('about').'#conocenos-mas',
    'menu-4-4-url' => route('about').'#nuestros-logros',
    'menu-5' => 'Materiales',
    'menu-5-1' => 'Ladrillos',
    'menu-5-2' => 'Celosías',
    'menu-5-3' => 'Tejas',
    'menu-5-4' => 'Pisos',
    'menu-5-5' => 'Fachaletas',
    'menu-5-1-url' => route('materials') . '#ladrillos',
    'menu-5-2-url' => route('materials') . '#celosias',
    'menu-5-3-url' => route('materials') . '#tejas',
    'menu-5-4-url' => route('materials') . '#pisos',
    'menu-5-5-url' => route('materials') . '#fachaletas',
    'menu-6' => 'Contacto',
    'menu-7' => 'Idioma',
    'search-ph' => 'Escribe y presiona Intro',

];