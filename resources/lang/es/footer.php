<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Footer Language Lines
    |--------------------------------------------------------------------------
    |
    | Las siguientes lineas son traducciones del sitio, forman parte del diccionario
    | que proporciona las interpretaciones multi-idioma.
    |
 */

    'title-1' => 'Acerca de Nosotros',
    'title-2' => 'Accesos Rápidos',
    'title-3' => 'Contáctanos',
    'quick-link-1' => 'Hornos',
    'quick-link-2' => 'Cúpulas',
    'quick-link-3' => 'Chimeneas',
    'quick-link-4' => 'Fachadas',
    'quick-link-5' => 'Asadores',
    'quick-link-6' => 'Cavas',
    'contact-p' => 'Si tienes dudas o quieres solicitar una cotización, sin compromiso, contáctanos en los siguientes medios:',
    'copy-right' => 'CaboBrickDomes 2018 Copyright &copy;. Todos los derechos Reservados. Sitio Optimizado por',

];