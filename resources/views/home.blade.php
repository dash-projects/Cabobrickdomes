@extends('layouts.base')
@section('title',trans('header.menu-1'))
@section('header')
<style media="screen">
  /*
    font-family: 'Open Sans', sans-serif;
    font-family: 'Oswald', sans-serif;
    font-family: 'Raleway', sans-serif;
    font-family: 'Montserrat', sans-serif;
  */
  .text-os {
    font-family: 'Open Sans', sans-serif;
  }
  .text-od{
    font-family: 'Oswald', sans-serif;
  }
  .text-ry{
    font-family: 'Raleway', sans-serif;
  }
  .text-mt{
    font-family: 'Montserrat', sans-serif;
  }

</style>
@endsection
@section('content')
@include('miselaneous.slider')
{{-- @include('theme2-section.slider') --}}
  {{-- @include('theme2-section.offers') --}}
  @include('theme2-section.work')
  {{--  @include('theme2-section.testimonial')  --}}
@endsection
@section('scripts')
<!-- Animate JS -->
<script src="vendors/animate/wow.min.js"></script>
<!-- Stellar JS -->
<script src="vendors/stellar/jquery.stellar.min.js"></script>
<!-- Theme JS -->
<script src="js/theme.min.js"></script>
@endsection
