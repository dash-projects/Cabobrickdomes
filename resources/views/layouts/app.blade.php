<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" class="no-js">
<head>
  <!-- Mobile Specific Meta -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Favicon-->
  <link rel="shortcut icon" href="img/fav.png">
  <!-- Author Meta -->
  <meta name="author" content="Martin Urias">
  <!-- Meta Description -->
  <meta name="description"|| content="Empresa de Construcción con ladrillo, Acabados de ladrillo. Realizamos Cupulas, Hornos, Chimeneas y trabajos de ladrillo. Especializados en domos o cúpulas de ladrillo. en Baja California Sur, México.">
  <!-- Meta Keyword -->
  <meta name="keywords" content="ladrillo,bricks,Los Cabos,Baja California Sur, Cabo San Lucas, Cabo, Construction Brick, Construcción Ladrillo, Cupulas, Domos, Domes, Hornos, Oven, Chimeneas, FirePlace, México">
  <!-- meta character set -->
  <meta charset="UTF-8">
  <link rel="apple-touch-icon" sizes="180x180" href="/img/ico/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/img/ico/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/img/ico/favicon-16x16.png">
  <link rel="manifest" href="/img/ico/site.webmanifest">
  <link rel="mask-icon" href="/img/ico/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">


  <!-- Site Title -->
  <title>@yield('title') - Cabo Brick Domes</title>
  <link href="https://fonts.googleapis.com/css?family=Montserrat|Open+Sans|Oswald|Raleway" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
    <!--
    CSS
    ============================================= -->
    <link rel="stylesheet" href="css/linearicons.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/main.css">
    @yield('header')
  </head>
  <body>

      @yield('content')

    <script src="js/vendor/jquery-2.2.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
    <script src="js/easing.min.js"></script>
    <script src="js/hoverIntent.js"></script>
    <script src="js/superfish.min.js"></script>
    <!-- <script src="js/jquery.ajaxchimp.min.js"></script> -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/parallax.min.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/mail-script.js"></script>
    <script src="js/main.js"></script>
    @yield('script')
  </body>
</html>
