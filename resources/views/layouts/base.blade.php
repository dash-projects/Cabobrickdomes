<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" class="no-js">

<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/fav.png">
    <!-- Author Meta -->
    <meta name="author" content="Martin Urias">
    <!-- Meta Description -->
    <meta name="description" content="Empresa de Construcción con ladrillo, Acabados de ladrillo. Realizamos Cupulas, Hornos, Chimeneas y trabajos de ladrillo. Especializados en domos o cúpulas de ladrillo. en Baja California Sur, México.">
    <!-- Meta Keyword -->
    <meta name="keywords" content="ladrillo,bricks,Los Cabos,Baja California Sur, Cabo San Lucas, Cabo, Construction Brick, Construcción Ladrillo, Cupulas, Domos, Domes, Hornos, Oven, Chimeneas, FirePlace, México">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/ico/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/ico/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/ico/favicon-16x16.png">
    <link rel="manifest" href="/img/ico/site.webmanifest">
    <link rel="mask-icon" href="/img/ico/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">


    <!-- Site Title -->
    <title>@yield('title') - Cabo Brick Domes</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Open+Sans|Oswald|Raleway" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Animate CSS -->
    {{--  <link href="vendors/animate/animate.css" rel="stylesheet">  --}}
    <!-- Icon CSS-->
	<link rel="stylesheet" href="vendors/font-awesome/css/font-awesome.min.css">
    <!-- Camera Slider -->
    {{--  <link rel="stylesheet" href="vendors/camera-slider/camera.css">  --}}
    <!-- Owlcarousel CSS-->
	{{--  <link rel="stylesheet" type="text/css" href="vendors/owl_carousel/owl.carousel.css" media="all">  --}}

    <!--Theme Styles CSS-->
	<link rel="stylesheet" type="text/css" href="css/style.css" media="all" />
    <!--Master Slider Styles CSS-->
    <link href="/masterslider/style/masterslider.css" type="text/css" rel="stylesheet">
    <link href="/masterslider/skins/default/style.css" type="text/css" rel="stylesheet">
    
    {{--  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->  --}}
    <style>
    body{
        min-width: 345px;
    }
    </style>
    @yield('header')
</head>
<body>
    <!-- Preloader -->
    {{--  <div class="preloader"></div>  --}}
     @include('theme2-section.top-header') 
    @include('theme2-section.header')
    @yield('content')
    @include('theme2-section.footer')
    <!-- jQuery JS -->
    <script src="js/jquery-1.12.0.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="js/bootstrap.min.js"></script>
    @yield('scripts')
</body>
</html>
