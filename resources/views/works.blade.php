@extends('layouts.base')
@section('title',trans('header.menu-3'))
@section('header')
<style media="screen">
  /*
    font-family: 'Open Sans', sans-serif;
    font-family: 'Oswald', sans-serif;
    font-family: 'Raleway', sans-serif;
    font-family: 'Montserrat', sans-serif;
  */
  .text-os {
    font-family: 'Open Sans', sans-serif;
  }
  .text-od{
    font-family: 'Oswald', sans-serif;
  }
  .text-ry{
    font-family: 'Raleway', sans-serif;
  }
  .text-mt{
    font-family: 'Montserrat', sans-serif;
  }

</style>
 <!--[if lt IE 9]>
    <script src="js/html5shiv.min.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link  href="/vendors/fancybox-master/dist/jquery.fancybox.min.css" rel="stylesheet">
@endsection
@section('content')
   {{-- @include('theme2-section.work') --}}
   @include('theme2-section.porfolio')
@endsection
@section('scripts')
    <!-- Animate JS -->
    <script src="vendors/animate/wow.min.js"></script>
    <!-- Stellar JS -->
    <script src="vendors/stellar/jquery.stellar.js"></script>
    <!-- Camera Slider -->
    <script src="vendors/camera-slider/jquery.easing.1.3.js"></script>
    <script src="vendors/camera-slider/camera.min.js"></script>
    <!-- Isotope JS -->
    <script src="vendors/isotope/imagesloaded.pkgd.min.js"></script>
    <script src="vendors/isotope/isotope.pkgd.min.js"></script>
    <!-- Progress JS -->
    <script src="vendors/Counter-Up/jquery.counterup.min.js"></script>
    <script src="vendors/Counter-Up/waypoints.min.js"></script>
    <!-- Owlcarousel JS -->
    <script src="vendors/owl_carousel/owl.carousel.min.js"></script>
    <!-- Theme JS -->
    <script src="js/theme.js"></script>
    <script src="/vendors/fancybox-master/dist/jquery.fancybox.min.js"></script>

@endsection
