<!-- Our Features Area -->
<section id="{{trans('content.about-section-id-3')}}" class="our_feature_area">
        <div class="container">
            <div class="tittle wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                <h2>{{trans('content.about-learn-more-title')}}</h2>
                <h4></h4>
            </div>
            <div class="feature_row row">
                <div class="col-md-6 feature_img">
                    <img src="img/about-2.jpg" alt="">
                </div>
                <div class="col-md-6 feature_content">
                    <div class="subtittle">
                        <h2>{{trans('content.about-learn-more-subtitle')}}</h2>
                        <h5>{{trans('content.about-learn-more-desc')}}</h5>
                    </div>
                    <div class="media">
                        <div class="media-left">
                            <a href="#">
                                <i class="fa fa-wrench" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="media-body">
                            <a href="#">{{trans('content.about-learn-more-1')}}</a>
                            <p>{{trans('content.about-learn-more-1-desc')}}</p>
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left">
                            <a href="#">
                                <i class="fa fa-rocket" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="media-body">
                            <a href="#">{{trans('content.about-learn-more-2')}}</a>
                            <p>{{trans('content.about-learn-more-2-desc')}}</p>
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left">
                            <a href="#">
                                <i class="fa fa-users" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="media-body">
                            <a href="#">{{trans('content.about-learn-more-3')}}</a>
                            <p>{{trans('content.about-learn-more-3-desc')}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Our Features Area -->
