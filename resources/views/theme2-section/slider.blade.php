 <!-- Slider area -->
    <section class="slider_area row m0">
        <div class="slider_inner">
            <div data-thumb="img/slider-1.jpg" data-src="img/slider-1.jpg">
                <div class="camera_caption">
                   <div class="container">
                        <h2 class=" wow fadeInUp animated">{{trans('content.slider1-title')}}</h2>
                        <h2 class=" wow fadeInUp animated" data-wow-delay="0.5s">{{trans('content.slider1-subtitle1')}}</h2>
                        <p class=" wow fadeInUp animated" data-wow-delay="0.8s">{{trans('content.slider1-subtitle2')}}</p>
                        <a class=" wow fadeInUp animated" data-wow-delay="1s" href="{{trans('content.slider1-url')}}">{{trans('content.slider1-btn')}}</a>
                   </div>
                </div>
            </div>
            <div data-thumb="img/slider-2.jpg" data-src="img/slider-2.jpg">
                <div class="camera_caption">
                   <div class="container">
                        <h2 class=" wow fadeInUp animated">{{trans('content.slider2-title')}}</h2>
                        <h2 class=" wow fadeInUp animated" data-wow-delay="0.5s">{{trans('content.slider2-subtitle1')}}</h2>
                        <p class=" wow fadeInUp animated" data-wow-delay="0.8s">{{trans('content.slider2-subtitle2')}}</p>
                        <a class=" wow fadeInUp animated" data-wow-delay="1s" href="{{trans('content.slider2-url')}}">{{trans('content.slider2-btn')}}</a>
                   </div>
                </div>
            </div>
            <div data-thumb="img/slider-3.png" data-src="img/slider-3.png">
                <div class="camera_caption">
                   <div class="container">
                        <h2 class=" wow fadeInUp animated">{{trans('content.slider3-title')}}</h2>
                        <h2 class=" wow fadeInUp animated" data-wow-delay="0.5s">{{trans('content.slider3-subtitle1')}}</h2>
                        <p class=" wow fadeInUp animated" data-wow-delay="0.8s">{{trans('content.slider3-subtitle2')}}</p>
                        <a class=" wow fadeInUp animated" data-wow-delay="1s" href="{{trans('content.slider3-url')}}">{{trans('content.slider3-btn')}}</a>
                   </div>
                </div>
            </div>
             <div data-thumb="img/slider-4.jpg" data-src="img/slider-4.jpg">
                <div class="camera_caption">
                   <div class="container">
                        <h2 class=" wow fadeInUp animated">{{trans('content.slider4-title')}}</h2>
                        <h2 class=" wow fadeInUp animated" data-wow-delay="1.5s">{{trans('content.slider4-subtitle1')}}</h2>
                        <p class=" wow fadeInUp animated" data-wow-delay="0.8s">{{trans('content.slider4-subtitle2')}}</p>
                        <a class=" wow fadeInUp animated" data-wow-delay="1s" href="{{trans('content.slider4-url')}}">{{trans('content.slider4-btn')}}</a>
                   </div>
                </div>
            </div>
             <div data-thumb="img/slider-5.jpg" data-src="img/slider-5.jpg">
                <div class="camera_caption">
                   <div class="container">
                        <h2 class=" wow fadeInUp animated">{{trans('content.slider5-title')}}</h2>
                        <h2 class=" wow fadeInUp animated" data-wow-delay="1.5s">{{trans('content.slider5-subtitle1')}}</h2>
                        <p class=" wow fadeInUp animated" data-wow-delay="0.8s">{{trans('content.slider5-subtitle2')}}</p>
                        <a class=" wow fadeInUp animated" data-wow-delay="1s" href="{{trans('content.slider5-url')}}">{{trans('content.slider5-btn')}}</a>
                   </div>
                </div>
            </div>
             <div data-thumb="img/slider-6.jpg" data-src="img/slider-6.jpg">
                <div class="camera_caption">
                   <div class="container">
                        <h2 class=" wow fadeInUp animated">{{trans('content.slider6-title')}}</h2>
                        <h2 class=" wow fadeInUp animated" data-wow-delay="1.5s">{{trans('content.slider6-subtitle1')}}</h2>
                        <p class=" wow fadeInUp animated" data-wow-delay="0.8s">{{trans('content.slider6-subtitle2')}}</p>
                        <a class=" wow fadeInUp animated" data-wow-delay="1s" href="{{trans('content.slider6-url')}}">{{trans('content.slider6-btn')}}</a>
                   </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Slider area -->