@foreach($sliders as $slider )
<!-- Slide {{$slider['title']}} area -->
<div class="container">
  <h2>{{$slider['title']}}</h2>  
  <div id="{{$slider['id']}}" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
    @foreach($slider['images'] as $clave => $image)
        @if($clave == 0)
        <li data-target="{{$slider['id']}}" data-slide-to="{{$clave}}" class="active"></li>
        @else
        <li data-target="{{$slider['id']}}" data-slide-to="{{$clave}}"></li>
        @endif
    @endforeach
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
    @foreach($slider['images'] as $clave => $image)
        @if($clave == 0)
        <div class="item active">
        @else
        <div class="item">
        @endif
            <img class="slider" src="{{$image['url']}}" alt="{{$image['title']}}">
        </div>
    @endforeach
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#{{$slider['id']}}" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only"><</span>
    </a>
    <a class="right carousel-control" href="#{{$slider['id']}}" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">></span>
    </a>
  </div>
</div>
<!-- End Slider area -->
<br>
<hr class="divider">
<br>
@endforeach
