  <!-- Our Featured Works Area -->
    <section id="{{trans('content.work-type-id')}}" class="featured_works row" data-stellar-background-ratio="0.3">
        <div class="tittle wow fadeInUp">
            <h2>{{trans('content.work-type-title')}}</h2>
            <h4>{{trans('content.work-type-desc')}}</h4>
        </div>
        <div class="featured_gallery">
            <div class="col-md-4 col-sm-6 col-xs-6 gallery_iner p0" onclick="window.location.href='{{trans('header.menu-3-1-url')}}';">
                <img src="/img/gallery/hornos/01.jpg" alt=" Trabajo Horno Pichuela">
                <div class="gallery_hover">
                    <a href="{{route('works')}}#hornos">{{trans('content.work-1')}}</a>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-6 gallery_iner p0" onclick="window.location.href='{{trans('header.menu-3-2-url')}}';">
                <img src="/img/gallery/cupulas_circulares/01.jpg" alt="Trabajo Cúpula Circular">
                <div class="gallery_hover">
                    <a href="{{route('works')}}#cupulas">{{trans('content.work-2')}}</a>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-6 gallery_iner p0" onclick="window.location.href='{{trans('header.menu-3-3-url')}}';">
                <img src="/img/gallery/chimeneas/01.jpg" alt="Trabajo de Chimenea">
                <div class="gallery_hover">
                    <a href="{{route('works')}}#chimenea">{{trans('content.work-3')}}</a>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-6 gallery_iner p0" onclick="window.location.href='{{trans('header.menu-3-4-url')}}';">
                <img src="/img/gallery/trabajos_ladrillo/01.jpg" alt="Trabajo de Fachada">
                <div class="gallery_hover">
                    <a href="{{route('works')}}#fachadas">{{trans('content.work-4')}}</a>
                </div>
            </div>
        </div>
        <div class="featured_gallery">
             <div class="col-md-4 col-sm-6 col-xs-6 gallery_iner p0" onclick="window.location.href='{{trans('header.menu-3-5-url')}}';">
                <img src="/img/asadores2.jpeg" alt="Trabajo de Asadores">
                <div class="gallery_hover">
                    <a href="{{route('works')}}#asadores">{{trans('content.work-5')}}</a>
                </div>
            </div>
        </div>
        <div class="featured_gallery">
             <div class="col-md-4 col-sm-6 col-xs-6 gallery_iner p0" onclick="window.location.href='{{trans('header.menu-3-6-url')}}';">
                <img src="/img/cava1.jpeg" alt="Trabajo de Cavas">
                <div class="gallery_hover">
                    <a href="{{route('works')}}#cavas">{{trans('content.work-6')}}</a>
                </div>
            </div>
        </div>
    </section>
    <!-- End Our Featured Works Area -->
