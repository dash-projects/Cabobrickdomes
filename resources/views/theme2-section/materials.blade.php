
<section id="{{trans('content.material-id',['mat'=>trans_choice('content.material-brick',2)])}}" class="about_us_area row">
    <div class="container">
        <div class="tittle wow fadeInUp">
            <h2>{{trans('content.material-title',['mat'=>trans_choice('content.material-brick',2)])}}</h2>
            <h4>{{trans('content.material-desc',['mat'=>trans_choice('content.material-brick',2)])}}</h4>
        </div>
        <div class="row">
            <!-- blog-2 area -->
            <section class="blog_tow_area">
                <div class="container">
                    <div class="row blog_tow_row">
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/1022.png">
                                    <img src="/img/materials/1022.png" alt="{{trans('content.material-brick-1')}}" title="{{trans('content.material-brick-1')}}">
                                </a>
                                <div class="renovation_content">
                                    <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-brick-1')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 120.00 - 240.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-bookmark" aria-hidden="true"></i> {{trans('content.material-code')}} <em>1022</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 6 x 12 x 24 cm <br>{!!trans('content.material-quantity',['quantity'=>60])!!}<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/1218.png">
                                    <img src="/img/materials/1218.png" alt="{{trans('content.material-brick-2')}}" title="{{trans('content.material-brick-2')}}">
                                </a>
                                <div class="renovation_content">
                                <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-brick-2')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 100.00 - 220.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>1218</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 6 x 12 x 24 cm <br>{!!trans('content.material-quantity',['quantity'=>60])!!}<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/1222.png">
                                    <img src="/img/materials/1222.png" alt="{{trans('content.material-brick-3')}}" title="{{trans('content.material-brick-3')}}">
                                </a>
                                <div class="renovation_content">
                                    <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-brick-3')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 120.00 - 240.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>1222</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 6 x 12 x 24 cm <br>{!!trans('content.material-quantity',['quantity'=>60])!!}<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/1215.png">
                                    <img src="/img/materials/1215.png" alt="{{trans('content.material-brick-4')}}" title="{{trans('content.material-brick-4')}}">
                                </a>
                                <div class="renovation_content">
                                    <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-brick-4')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 120.00 - 240.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>1215</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 7 x 14 x 30 cm <br>{!!trans('content.material-quantity',['quantity'=>48])!!}<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/1216.png">
                                    <img src="/img/materials/1216.png" alt="{{trans('content.material-brick-5')}}" title="{{trans('content.material-brick-5')}}">
                                </a>
                                <div class="renovation_content">
                                <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-brick-5')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 100.00 - 220.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>1216</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 7 x 14 x 33 cm <br>{!!trans('content.material-quantity',['quantity'=>45])!!}<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/1217.png">
                                    <img src="/img/materials/1217.png" alt="{{trans('content.material-brick-6')}}" title="{{trans('content.material-brick-6')}}">
                                </a>
                                <div class="renovation_content">
                                <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-brick-6')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 100.00 - 220.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>1217</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 4 x 10 x 20 cm <br>{!!trans('content.material-quantity',['quantity'=>110])!!}<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/1208.png">
                                    <img src="/img/materials/1208.png" alt="{{trans('content.material-brick-7')}}" title="{{trans('content.material-brick-7')}}">
                                </a>
                                <div class="renovation_content">
                                <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-brick-7')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 100.00 - 220.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>1208</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 5.5 x 9.5 x 19 cm <br>{!!trans('content.material-quantity',['quantity'=>100])!!}<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/1210.png">
                                    <img src="/img/materials/1210.png" alt="{{trans('content.material-brick-8')}}" title="{{trans('content.material-brick-8')}}">
                                </a>
                                <div class="renovation_content">
                                <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-brick-8')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 100.00 - 220.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>1210</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 5 x 11.5 x 22 cm <br>{!!trans('content.material-quantity',['quantity'=>80])!!}<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/1236.png">
                                    <img src="/img/materials/1236.png" alt="{{trans('content.material-brick-9')}}" title="{{trans('content.material-brick-9')}}">
                                </a>
                                <div class="renovation_content">
                                <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-brick-9')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 100.00 - 220.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>1236</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 6 x 11 x 23 cm <br>{!!trans('content.material-quantity',['quantity'=>70])!!}<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/1226.png">
                                    <img src="/img/materials/1226.png" alt="{{trans('content.material-brick-10')}}" title="{{trans('content.material-brick-10')}}">
                                </a>
                                <div class="renovation_content">
                                <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-brick-10')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 100.00 - 220.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>1226</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 6 x 10 x 20 cm <br>{!!trans('content.material-quantity',['quantity'=>90])!!}<br></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End blog-2 area -->
        </div>
    </div>
</section>
<section id="{{trans('content.material-id',['mat'=>trans_choice('content.material-lattice',2)])}}" class="about_us_area row">
    <div class="container">
        <div class="tittle wow fadeInUp">
            <h2>{{trans('content.material-title',['mat'=>trans_choice('content.material-lattice',2)])}}</h2>
            <h4>{{trans('content.material-desc',['mat'=>trans_choice('content.material-lattice',2)])}}</h4>
        </div>
        <div class="row">
            <!-- blog-2 area -->
            <section class="blog_tow_area">
                <div class="container">
                    <div class="row blog_tow_row">
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/celosia_1120_1.png">
                                    <img src="/img/materials/celosia_1120_1.png" alt="{{trans('content.material-lattice-1')}}" title="{{trans('content.material-lattice-1')}}">
                                </a>
                                <div class="renovation_content">
                                <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-lattice-1')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 100.00 - 220.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>1120</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 6 x 10 x 20 cm <br>{!!trans('content.material-quantity',['quantity'=>32])!!}<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/celosia_1120_2.png">
                                    <img src="/img/materials/celosia_1120_2.png" alt="{{trans('content.material-lattice-2')}}" title="{{trans('content.material-lattice-2')}}">
                                </a>
                                <div class="renovation_content">
                                <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-lattice-2')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 100.00 - 220.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>1120</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 6 x 10 x 20 cm <br>{!!trans('content.material-quantity',['quantity'=>32])!!}<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/celosia_1120_3.png">
                                    <img src="/img/materials/celosia_1120_3.png" alt="{{trans('content.material-lattice-3')}}" title="{{trans('content.material-lattice-3')}}">
                                </a>
                                <div class="renovation_content">
                                <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-lattice-3')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 100.00 - 220.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>1120</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 6 x 10 x 20 cm <br>{!!trans('content.material-quantity',['quantity'=>32])!!}<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/celosia_1120_4.png">
                                    <img src="/img/materials/celosia_1120_4.png" alt="{{trans('content.material-lattice-4')}}" title="{{trans('content.material-lattice-4')}}">
                                </a>
                                <div class="renovation_content">
                                <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-lattice-4')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 100.00 - 220.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>1120</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 6 x 10 x 20 cm <br>{!!trans('content.material-quantity',['quantity'=>32])!!}<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/celosia_1120_5.png">
                                    <img src="/img/materials/celosia_1120_5.png" alt="{{trans('content.material-lattice-5')}}" title="{{trans('content.material-lattice-5')}}">
                                </a>
                                <div class="renovation_content">
                                <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-lattice-5')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 100.00 - 220.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>1120</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 6 x 10 x 20 cm <br>{!!trans('content.material-quantity',['quantity'=>32])!!}<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/celosia_1120_6.png">
                                    <img src="/img/materials/celosia_1120_6.png" alt="{{trans('content.material-lattice-6')}}" title="{{trans('content.material-lattice-6')}}">
                                </a>
                                <div class="renovation_content">
                                <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-lattice-6')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 100.00 - 220.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>1120</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 6 x 10 x 20 cm <br>{!!trans('content.material-quantity',['quantity'=>32])!!}<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/celosia_1120_7.png">
                                    <img src="/img/materials/celosia_1120_7.png" alt="{{trans('content.material-lattice-7')}}" title="{{trans('content.material-lattice-7')}}">
                                </a>
                                <div class="renovation_content">
                                <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-lattice-7')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 100.00 - 220.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>1020</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 6 x 10 x 20 cm <br>{!!trans('content.material-quantity',['quantity'=>32])!!}<br></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End blog-2 area -->
        </div>
    </div>
</section>
<section id="{{trans('content.material-id',['mat'=>trans_choice('content.material-roof',2)])}}" class="about_us_area row">
    <div class="container">
        <div class="tittle wow fadeInUp">
            <h2>{{trans('content.material-title',['mat'=>trans_choice('content.material-roof',2)])}}</h2>
            <h4>{{trans('content.material-desc',['mat'=>trans_choice('content.material-roof',2)])}}</h4>
        </div>
        <div class="row">
            <!-- blog-2 area -->
            <section class="blog_tow_area">
                <div class="container">
                    <div class="row blog_tow_row">
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/teja_15347.png">
                                    <img src="/img/materials/teja_15347.png" alt="{{trans('content.material-roof-1')}}" title="{{trans('content.material-roof-1')}}">
                                </a>
                                <div class="renovation_content">
                                <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-roof-1')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 100.00 - 220.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>15347</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 42 x 15 cm <br>{!!trans('content.material-quantity',['quantity'=>24])!!}<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/teja_15355.png">
                                    <img src="/img/materials/teja_15355.png" alt="{{trans('content.material-roof-2')}}" title="{{trans('content.material-roof-2')}}">
                                </a>
                                <div class="renovation_content">
                                <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-roof-2')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 100.00 - 220.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>15355</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 32 x 15 cm <br>{!!trans('content.material-quantity',['quantity'=>36])!!}<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/teja_15230.png">
                                    <img src="/img/materials/teja_15230.png" alt="{{trans('content.material-roof-3')}}" title="{{trans('content.material-roof-3')}}">
                                </a>
                                <div class="renovation_content">
                                <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-roof-3')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 100.00 - 220.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>15230</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 45 x 17 cm <br>{!!trans('content.material-quantity',['quantity'=>20])!!}<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/teja_15389.png">
                                    <img src="/img/materials/teja_15389.png" alt="{{trans('content.material-roof-4')}}" title="{{trans('content.material-roof-4')}}">
                                </a>
                                <div class="renovation_content">
                                <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-roof-4')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 100.00 - 220.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>15389</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 46 x 30 cm <br>{!!trans('content.material-quantity',['quantity'=>10])!!}<br></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End blog-2 area -->
        </div>
    </div>
</section>
<section id="{{trans('content.material-id',['mat'=>trans_choice('content.material-floor',2)])}}" class="about_us_area row">
    <div class="container">
        <div class="tittle wow fadeInUp">
            <h2>{{trans('content.material-title',['mat'=>trans_choice('content.material-floor',2)])}}</h2>
            <h4>{{trans('content.material-desc',['mat'=>trans_choice('content.material-floor',2)])}}</h4>
        </div>
        <div class="row">
            <!-- blog-2 area -->
            <section class="blog_tow_area">
                <div class="container">
                    <div class="row blog_tow_row">
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/piso_70277.png">
                                    <img src="/img/materials/piso_70277.png" alt="{{trans('content.material-floor-1')}}" title="{{trans('content.material-floor-1')}}">
                                </a>
                                <div class="renovation_content">
                                <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-floor-1')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 100.00 - 220.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>70277</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 30 x 30 cm <br>{!!trans('content.material-quantity',['quantity'=>11])!!}<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/piso_90379.png">
                                    <img src="/img/materials/piso_90379.png" alt="{{trans('content.material-floor-2')}}" title="{{trans('content.material-floor-2')}}">
                                </a>
                                <div class="renovation_content">
                                <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-floor-2')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 100.00 - 220.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>90379</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 40 x 40 cm <br>{!!trans('content.material-quantity',['quantity'=>6])!!}<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/piso_70490.png">
                                    <img src="/img/materials/piso_70490.png" alt="{{trans('content.material-floor-3')}}" title="{{trans('content.material-floor-3')}}">
                                </a>
                                <div class="renovation_content">
                                <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-floor-3')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 100.00 - 220.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>70490</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 30 x 45 cm <br>{!!trans('content.material-quantity',['quantity'=>7])!!}<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/piso_77690.png">
                                    <img src="/img/materials/piso_77690.png" alt="{{trans('content.material-floor-4')}}" title="{{trans('content.material-floor-4')}}">
                                </a>
                                <div class="renovation_content">
                                <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-floor-4')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 100.00 - 220.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>77690</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 60 x 60 cm <br>{!!trans('content.material-quantity',['quantity'=>3])!!}<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/piso_90275.png">
                                    <img src="/img/materials/piso_90275.png" alt="{{trans('content.material-floor-5')}}" title="{{trans('content.material-floor-5')}}">
                                </a>
                                <div class="renovation_content">
                                <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-floor-5')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 100.00 - 220.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>90275</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 20 x 20 cm <br>{!!trans('content.material-quantity',['quantity'=>25])!!}<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/piso_90270.png">
                                    <img src="/img/materials/piso_90270.png" alt="{{trans('content.material-floor-6')}}" title="{{trans('content.material-floor-6')}}">
                                </a>
                                <div class="renovation_content">
                                <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-floor-6')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 100.00 - 220.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>90270</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 30 x 30 cm <br>{!!trans('content.material-quantity',['quantity'=>11])!!}<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/piso_90370.png">
                                    <img src="/img/materials/piso_90370.png" alt="{{trans('content.material-floor-7')}}" title="{{trans('content.material-floor-7')}}">
                                </a>
                                <div class="renovation_content">
                                <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-floor-7')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 100.00 - 220.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>90370</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 40 x 40 cm <br>{!!trans('content.material-quantity',['quantity'=>6])!!}<br></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End blog-2 area -->
        </div>
    </div>
</section>
<section id="{{trans('content.material-id',['mat'=>trans_choice('content.material-facade',2)])}}" class="about_us_area row">
    <div class="container">
        <div class="tittle wow fadeInUp">
            <h2>{{trans('content.material-title',['mat'=>trans_choice('content.material-facade',2)])}}</h2>
            <h4>{{trans('content.material-desc',['mat'=>trans_choice('content.material-facade',2)])}}</h4>
        </div>
        <div class="row">
            <!-- blog-2 area -->
            <section class="blog_tow_area">
                <div class="container">
                    <div class="row blog_tow_row">
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/fachaleta_1707.png">
                                    <img src="/img/materials/fachaleta_1707.png" alt="{{trans('content.material-facade-1')}}" title="{{trans('content.material-facade-1')}}">
                                </a>
                                <div class="renovation_content">
                                <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-facade-1')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 100.00 - 220.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>1707</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 1.5 x 5 x 9 cm <br>{!!trans('content.material-quantity',['quantity'=>220])!!}<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/fachaleta_1807.png">
                                    <img src="/img/materials/fachaleta_1807.png" alt="{{trans('content.material-facade-2')}}" title="{{trans('content.material-facade-2')}}">
                                </a>
                                <div class="renovation_content">
                                <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-facade-2')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 100.00 - 220.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>1807</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 1.5 x 5 x 22 cm <br>{!!trans('content.material-quantity',['quantity'=>90])!!}<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/fachaleta_1120.png">
                                    <img src="/img/materials/fachaleta_1120.png" alt="{{trans('content.material-facade-3')}}" title="{{trans('content.material-facade-3')}}">
                                </a>
                                <div class="renovation_content">
                                <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-facade-3')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 100.00 - 220.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>1120</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 1.5 x 5 x 20 cm <br>{!!trans('content.material-quantity',['quantity'=>100])!!}<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/fachaleta_1422.png">
                                    <img src="/img/materials/fachaleta_1422.png" alt="{{trans('content.material-facade-4')}}" title="{{trans('content.material-facade-4')}}">
                                </a>
                                <div class="renovation_content">
                                <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-facade-4')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 100.00 - 220.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>1422</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 1.5 x 10 x 20 cm <br>{!!trans('content.material-quantity',['quantity'=>50])!!}<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/fachaleta_1007_1.png">
                                    <img src="/img/materials/fachaleta_1007_1.png" alt="{{trans('content.material-facade-5')}}" title="{{trans('content.material-facade-5')}}">
                                </a>
                                <div class="renovation_content">
                                <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-facade-5')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 100.00 - 220.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>1007</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 1.5 x 6 x 22 cm <br>{!!trans('content.material-quantity',['quantity'=>60])!!}<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/fachaleta_1007_2.png">
                                    <img src="/img/materials/fachaleta_1007_2.png" alt="{{trans('content.material-facade-6')}}" title="{{trans('content.material-facade-6')}}">
                                </a>
                                <div class="renovation_content">
                                <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-facade-6')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 100.00 - 220.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>1007</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 1.5 x 6 x 22 cm <br>{!!trans('content.material-quantity',['quantity'=>60])!!}<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/fachaleta_1007_3.png">
                                    <img src="/img/materials/fachaleta_1007_3.png" alt="{{trans('content.material-facade-7')}}" title="{{trans('content.material-facade-7')}}">
                                </a>
                                <div class="renovation_content">
                                <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-facade-7')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 100.00 - 220.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>1007</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 1.5 x 6 x 22 cm <br>{!!trans('content.material-quantity',['quantity'=>60])!!}<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/fachaleta_1012_1.png">
                                    <img src="/img/materials/fachaleta_1012_1.png" alt="{{trans('content.material-facade-8')}}" title="{{trans('content.material-facade-8')}}">
                                </a>
                                <div class="renovation_content">
                                <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-facade-8')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 100.00 - 220.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>1012</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 1.5 x 6 x 22 cm <br>{!!trans('content.material-quantity',['quantity'=>60])!!}<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="renovation">
                                <a data-fancybox="gallery" href="/img/materials/fachaleta_1012_2.png">
                                    <img src="/img/materials/fachaleta_1012_2.png" alt="{{trans('content.material-facade-9')}}" title="{{trans('content.material-facade-9')}}">
                                </a>
                                <div class="renovation_content">
                                <a class="clipboard" href="#"><i class="fa fa-cube" aria-hidden="true"></i></a>
                                    <a class="tittle" href="#">{{trans('content.material-facade-9')}}</a>
                                    <div class="date_comment">
                                        {{--  <a href="#"><i class="fa fa-dollar" aria-hidden="true"></i> 100.00 - 220.00 C/U </a><br>  --}}
                                        <a href="#"><i class="fa fa-reply" aria-hidden="true"></i>  {{trans('content.material-code')}} <em>1012</em></a>
                                    </div>
                                    <p>{{trans('content.material-dimension')}} 1.5 x 6 x 22 cm <br>{!!trans('content.material-quantity',['quantity'=>60])!!}<br></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End blog-2 area -->
        </div>
    </div>
</section>