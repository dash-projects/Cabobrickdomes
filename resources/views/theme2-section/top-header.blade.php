<!-- Top Header_Area -->
	<section class="top_header_area visible-lg-block visible-md-block">
	    <div class="row">
            <div class="col-md-9 col-sm-6 text-center">
                <ul class="nav navbar-nav top_nav ">
                    <li><a href="tel:+5216241795885"><i class="fa fa-phone"></i>+52 (624) 179 5885</a></li>
                    {{-- <li><a href="#"><i class="fa fa-phone"></i>+52 (624) 127 2778</a></li> --}}
                    {{-- <li><a href="#"><i class="fa fa-phone"></i>+52 (624) 358 9614</a></li> --}}
                    <li><a href="mailto:cupulasjlf@gmail.com"><i class="fa fa-envelope-o"></i>cupulasjlf@gmail.com</a></li>
                    <li><a href="#"><i class="fa fa-clock-o"></i>{{trans('header.days')}} 12:00 - 20:00</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6 text-center">
                <ul class="nav navbar-nav top_nav navbar-right">
                <li><a href="#"><i class="fa fa-globe" aria-hidden="true"></i>{{trans('header.menu-7')}}</a></li>
                    <li><a href="/">ESP.</a></li>
                    <li><a href="/en"> ENG.</a></li>
                </ul>
            </div>
	    </div>
	</section>
	<!-- End Top Header_Area -->