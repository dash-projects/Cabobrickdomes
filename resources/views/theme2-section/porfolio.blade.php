<section id="trabajos" class="our_services_tow">
    <div class="container">
        <div class="architecture_area services_pages">
            <div class="portfolio_filter portfolio_filter_2">
                <ul>
                    <li data-filter="*" class="active" style="width:100%;"><a href=""><i class="fa fa-wrench" aria-hidden="true"></i>{{trans('content.work-filter-1')}}</a></li>
                    <li data-filter=".hornos" class=""><a href=""><i class="fa fa-fire" aria-hidden="true"></i>{{trans('content.work-filter-2')}}</a></li>
                    <li data-filter=".cupulas" class=""><a href=""><i class="fa fa-circle" aria-hidden="true"></i>{{trans('content.work-filter-3')}}</a></li>
                    <li data-filter=".chimeneas" class=""><a href=""><i class="fa fa-fire" aria-hidden="true"></i>{{trans('content.work-filter-4')}}</a></li>
                    <li data-filter=".fachadas" class=""><a href=""><i class="fa fa-building" aria-hidden="true"></i>{{trans('content.work-filter-5')}}</a></li>
                    <li data-filter=".asadores" class=""><a href=""><i class="fa fa-fire" aria-hidden="true"></i>{{trans('content.work-filter-6')}}</a></li>
                    <li data-filter=".cavas" class=""><a href=""><i class="fa fa-glass" aria-hidden="true"></i>{{trans('content.work-filter-7')}}</a></li>
                </ul>
            </div>
            <div id="{{trans('content.work-id-ovens')}}" class="portfolio_item portfolio_2">
                <div class="grid-sizer-2"></div>
                <div class="single_facilities col-sm-7 hornos" >
                    <div class="who_we_area">
                        <div class="subtittle">
                            <h2>{{trans('content.work-title-product-1')}}</h2>
                        </div>
                        <p>{{trans('content.work-desc-product-1')}}</p>
                        <a href="{{trans('header.menu-4-2-url')}}" class="button_all">{{trans('content.work-btn')}}</a>
                    </div>
                </div>
                <div class="single_facilities col-sm-5 hornos" >
                    <a data-fancybox="gallery" href="/img/compress/horno-pichuela.jpg">
                        <img src="/img/compress/horno-pichuela.jpg" alt="{{trans('content.work-title-product-1')}}" title="{{trans('content.work-title-product-1')}}">
                    </a>
                </div>
            </div>
            <div id="{{trans('content.work-id-domes')}}" class="portfolio_item portfolio_2">
                <div class="grid-sizer-2"></div>
                <div class="single_facilities col-sm-7 cupulas" >
                    <div class="who_we_area">
                        <div class="subtittle">
                            <h2>{{trans('content.work-title-product-2')}}</h2>
                        </div>
                        <p>{{trans('content.work-desc-product-2')}}</p>
                        <a href="{{trans('header.menu-4-2-url')}}" class="button_all">{{trans('content.work-btn')}}</a>
                    </div>
                </div>
                <div class="single_facilities col-sm-5 cupulas" >
                    <a data-fancybox="gallery" href="/img/compress/cupula-ovalo-1.jpg">
                        <img src="/img/compress/cupula-ovalo-1.jpg" alt="{{trans('content.work-title-product-2')}}" title="{{trans('content.work-title-product-2')}}">
                    </a>
                </div>
            </div>
            <div id="{{trans('content.work-id-fireplaces')}}" class="portfolio_item portfolio_2">
                <div class="grid-sizer-2"></div>
                <div class="single_facilities col-sm-7 chimeneas" >
                    <div class="who_we_area">
                        <div class="subtittle">
                            <h2>{{trans('content.work-title-product-3')}}</h2>
                        </div>
                        <p>{{trans('content.work-desc-product-3')}}</p>
                        <a href="{{trans('header.menu-4-2-url')}}" class="button_all">{{trans('content.work-btn')}}</a>
                    </div>
                </div>
                <div class="single_facilities col-sm-5 chimeneas" >
                    <a data-fancybox="gallery" href="/img/compress/chimenea1.jpg">
                        <img src="/img/compress/chimenea1.jpg" alt="{{trans('content.work-title-product-3')}}" title="{{trans('content.work-title-product-3')}}">
                    </a>
                </div>
            </div>
            <div id="{{trans('content.work-id-facades')}}" class="portfolio_item portfolio_2">
                <div class="grid-sizer-2"></div>
                <div class="single_facilities col-sm-7 fachadas" >
                    <div class="who_we_area">
                        <div class="subtittle">
                            <h2>{{trans('content.work-title-product-4')}}</h2>
                        </div>
                        <p>{{trans('content.work-desc-product-4')}}</p>
                        <a href="{{trans('header.menu-4-2-url')}}" class="button_all">{{trans('content.work-btn')}}</a>
                    </div>
                </div>
                <div class="single_facilities col-sm-5 fachadas" >
                    <a data-fancybox="gallery" href="/img/compress/fachada1.jpeg">
                        <img src="/img/compress/fachada1.jpeg" alt="{{trans('content.work-title-product-4')}}" title="{{trans('content.work-title-product-4')}}">
                    </a>
                </div>
            </div>
            <div id="{{trans('content.work-id-grill')}}" class="portfolio_item portfolio_2">
                <div class="grid-sizer-2"></div>
                <div class="single_facilities col-sm-7 asadores" >
                    <div class="who_we_area">
                        <div class="subtittle">
                            <h2>{{trans('content.work-title-product-5')}}</h2>
                        </div>
                        <p>{{trans('content.work-desc-product-5')}}</p>
                        <a href="{{trans('header.menu-4-2-url')}}" class="button_all">{{trans('content.work-btn')}}</a>
                    </div>
                </div>
                <div class="single_facilities col-sm-5 asadores" >
                    <a data-fancybox="gallery" href="/img/compress/asadores2.jpeg">
                        <img src="/img/compress/asadores2.jpeg" alt="{{trans('content.work-title-product-5')}}" title="{{trans('content.work-title-product-5')}}">
                    </a>
                </div>
            </div>
            <div id="{{trans('content.work-id-cellars')}}" class="portfolio_item portfolio_2">
                <div class="grid-sizer-2"></div>
                <div class="single_facilities col-sm-7 cavas" >
                    <div class="who_we_area">
                        <div class="subtittle">
                            <h2>{{trans('content.work-title-product-6')}}</h2>
                        </div>
                        <p>{{trans('content.work-desc-product-6')}}</p>
                        <a href="{{trans('header.menu-4-2-url')}}" class="button_all">{{trans('content.work-btn')}}</a>
                    </div>
                </div>
                <div class="single_facilities col-sm-5 cavas" >
                    <a data-fancybox="gallery" href="/img/compress/cava1.jpeg">
                        <img src="/img/compress/cava1.jpeg" alt="{{trans('content.work-title-product-6')}}" title="{{trans('content.work-title-product-6')}}">
                    </a>
                </div>
            </div>
            <div class="portfolio_item portfolio_2">
                <div class="grid-sizer-2"></div>
                <div class="single_facilities col-sm-7 cupulas" >
                    <div class="who_we_area">
                        <div class="subtittle">
                            <h2>{{trans('content.work-title-product-7')}}</h2>
                        </div>
                        <p>{{trans('content.work-desc-product-7')}}</p>
                        <a href="{{trans('header.menu-4-2-url')}}" class="button_all">{{trans('content.work-btn')}}</a>
                    </div>
                </div>
                <div class="single_facilities col-sm-5 cupulas" >
                    <a data-fancybox="gallery" href="/img/compress/cupula-ovalo-2.jpg">
                        <img src="/img/compress/cupula-ovalo-2.jpg" alt="{{trans('content.work-title-product-7')}}" title="{{trans('content.work-title-product-7')}}">
                    </a>
                </div>
            </div>
            <div class="portfolio_item portfolio_2">
                <div class="grid-sizer-2"></div>
                <div class="single_facilities col-sm-7 hornos" >
                    <div class="who_we_area">
                        <div class="subtittle">
                            <h2>{{trans('content.work-title-product-8')}}</h2>
                        </div>
                        <p>{{trans('content.work-desc-product-8')}}</p>
                        <a href="{{trans('header.menu-4-2-url')}}" class="button_all">{{trans('content.work-btn')}}</a>
                    </div>
                </div>
                <div class="single_facilities col-sm-5 hornos" >
                    <a data-fancybox="gallery" href="/img/compress/horno3.jpeg">
                        <img src="/img/compress/horno3.jpeg" alt="{{trans('content.work-title-product-8')}}" title="{{trans('content.work-title-product-8')}}">
                    </a>
                </div>
            </div>
             <div class="portfolio_item portfolio_2">
                <div class="grid-sizer-2"></div>
                <div class="single_facilities col-sm-7 cupulas" >
                    <div class="who_we_area">
                        <div class="subtittle">
                            <h2>{{trans('content.work-title-product-9')}}</h2>
                        </div>
                        <p>{{trans('content.work-desc-product-9')}}</p>
                        <a href="{{trans('header.menu-4-2-url')}}" class="button_all">{{trans('content.work-btn')}}</a>
                    </div>
                </div>
                <div class="single_facilities col-sm-5 cupulas" >
                    <a data-fancybox="gallery" href="/img/compress/cupula-circular1.jpeg">
                        <img src="/img/compress/cupula-circular1.jpeg" alt="{{trans('content.work-title-product-9')}}" title="{{trans('content.work-title-product-9')}}">
                    </a>
                </div>
            </div>
            
            <div class="portfolio_item portfolio_2">
                <div class="grid-sizer-2"></div>
                <div class="single_facilities col-sm-7 cupulas" >
                    <div class="who_we_area">
                        <div class="subtittle">
                            <h2>{{trans('content.work-title-product-10')}}</h2>
                        </div>
                        <p>{{trans('content.work-desc-product-10')}}</p>
                        <a href="{{trans('header.menu-4-2-url')}}" class="button_all">{{trans('content.work-btn')}}</a>
                    </div>
                </div>
                <div class="single_facilities col-sm-5 cupulas" >
                    <a data-fancybox="gallery" href="/img/compress/cupula-circular-3.jpeg">
                        <img src="/img/compress/cupula-circular-3.jpeg" alt="{{trans('content.work-title-product-10')}}" title="{{trans('content.work-title-product-10')}}">
                    </a>
                </div>
            </div>
            <div class="portfolio_item portfolio_2">
                <div class="grid-sizer-2"></div>
                <div class="single_facilities col-sm-7 cupulas" >
                    <div class="who_we_area">
                        <div class="subtittle">
                            <h2>{{trans('content.work-title-product-11')}}</h2>
                        </div>
                        <p>{{trans('content.work-desc-product-11')}}</p>
                        <a href="{{trans('header.menu-4-2-url')}}" class="button_all">{{trans('content.work-btn')}}</a>
                    </div>
                </div>
                <div class="single_facilities col-sm-5 cupulas" >
                    <a data-fancybox="gallery" href="/img/compress/boveda_canon1.jpg">
                        <img src="/img/compress/boveda_canon1.jpg" alt="{{trans('content.work-title-product-11')}}" title="{{trans('content.work-title-product-11')}}">
                    </a>
                </div>
            </div>
            {{--  <div id="temazcal" class="portfolio_item portfolio_2">
                <div class="grid-sizer-2"></div>
                <div class="single_facilities col-sm-7 temazcal" >
                    <div class="who_we_area">
                        <div class="subtittle">
                            <h2>{{trans('content.work-title-product-12')}}</h2>
                        </div>
                        <p>{{trans('content.work-desc-product-12')}}</p>
                        <a href="{{trans('header.menu-4-2-url')}}" class="button_all">{{trans('content.work-btn')}}</a>
                    </div>
                </div>
                <div class="single_facilities col-sm-5 temazcal" >
                    <a data-fancybox="gallery" href="images/horno1.jpeg">
                        <img src="images/horno1.jpeg" alt="{{trans('content.work-title-product-12')}}" title="{{trans('content.work-title-product-12')}}">
                    </a>
                </div>
            </div>
              --}}
           
        </div>
    </div>
</section>
@section('script')
@endsection