 <!-- Our Achievments Area -->
    <section id="{{trans('content.about-section-id-4')}}" class="our_achievments_area" data-stellar-background-ratio="0.3">
        <div class="container">
            <div class="tittle wow fadeInUp">
                <h2>{{trans('content.about-achiev-title')}}s</h2>
                <h4>{{trans('content.about-achiev-desc')}}</h4>
            </div>
            <div class="achievments_row row">
                <div class="col-md-4 col-sm-6 p0 completed">
                    <i class="fa fa-connectdevelop" aria-hidden="true"></i>
                    <span class="counter">250</span>
                    <h6>{{trans('content.about-achiev-1')}}</h6>
                </div>
                <div class="col-md-4 col-sm-6 p0 completed">
                    <i class="fa fa-home" aria-hidden="true"></i>
                    <span class="counter">60</span>
                    <h6>{{trans('content.about-achiev-2')}}</h6>
                </div>
                <div class="col-md-4 col-sm-6 p0 completed">
                    <i class="fa fa-trophy" aria-hidden="true"></i>
                    <span class="counter">125</span>
                    <h6>{{trans('content.about-achiev-3')}}</h6>
                </div>
            </div>
        </div>
    </section>
    <!-- End Our Achievments Area -->
