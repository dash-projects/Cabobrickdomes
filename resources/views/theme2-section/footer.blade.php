<!-- Footer Area -->
    <footer class="footer_area">
        <div class="container">
            <div class="footer_row row">
                <div class="col-md-4 col-sm-4 footer_about text-center">
                    <h2>{{trans('footer.title-1')}}</h2>
                    <img src="img/logo3.png" title="CABO BRICK DOMES" alt="CABO BRICK DOMES">
                    <ul class="socail_icon">
                        <li><a href="https://www.facebook.com/cabobrickdomes" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        {{-- <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li> --}}
                        <li><a href="https://www.instagram.com/cabobrickdomes" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
                <div class="col-md-4 col-sm-4 footer_about quick">
                    <h2>{{trans('footer.title-2')}}</h2>
                    <ul class="quick_link">
                        <li><a href="{{trans('header.menu-3-1-url')}}"><i class="fa fa-chevron-right"></i>{{trans('footer.quick-link-1')}}</a></li>
                        <li><a href="{{trans('header.menu-3-2-url')}}"><i class="fa fa-chevron-right"></i>{{trans('footer.quick-link-2')}}</a></li>
                        <li><a href="{{trans('header.menu-3-3-url')}}"><i class="fa fa-chevron-right"></i>{{trans('footer.quick-link-3')}}</a></li>
                        <li><a href="{{trans('header.menu-3-4-url')}}"><i class="fa fa-chevron-right"></i>{{trans('footer.quick-link-4')}}</a></li>
                        <li><a href="{{trans('header.menu-3-5-url')}}"><i class="fa fa-chevron-right"></i>{{trans('footer.quick-link-5')}}</a></li>
                        <li><a href="{{trans('header.menu-3-6-url')}}"><i class="fa fa-chevron-right"></i>{{trans('footer.quick-link-6')}}</a></li>
                    </ul>
                </div>
                <div class="col-md-4 col-sm-4 footer_about">
                    <h2>{{trans('footer.title-3')}}</h2>
                    <address>
                    <p>{{trans('footer.contact-p')}}</p>
                        <ul class="my_address">
                            <li><a href="mailto:cupulasjlf@gmail.com"><i class="fa fa-envelope-o"></i>cupulasjlf@gmail.com</a></li>
                            <li><a href="tel:+5216241795885"><i class="fa fa-phone" aria-hidden="true"></i>+52 (624) 179 5885</a></li>
                            {{--  <li><a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i><span>Sector # 10, Road # 05, Plot # 31, Uttara, Dhaka 1230 </span></a></li>  --}}
                        </ul>
                    </address>
                </div>
            </div>
        </div>
        <div class="copyright_area">
        {!!trans('footer.copy-right')!!} <a href="http://cabodash.com">Cabodash</a>
        </div>
    </footer>
    <!-- End Footer Area -->