 <!-- About Us Area -->
    <section id="{{trans('content.about-section-id-1')}}" class="about_us_area row">
        <div class="container">
            <div class="tittle wow fadeInUp">
                <h2>{{trans('content.about-title-1')}}</h2>
                <h4>{{trans('content.about-desc-1')}}</h4>
            </div>
            <div id="quienes-somos" class="row about_row">
                <div class="who_we_area col-md-7 col-sm-6">
                    <div class="subtittle">
                        <h2>{{trans('content.about-subtitle-1')}}</h2>
                    </div>
                    <p>{{trans('content.about-subdesc-1')}}</p>                    
                    <h2>{{trans('content.about-mision')}}</h2>
                    <p><i>{{trans('content.about-mision-desc')}}</i></p>
                    <h2>{{trans('content.about-vision')}}</h2>
                    <p> <i>{{trans('content.about-vision-desc')}}</i></p>
                    <h2>{{trans('content.about-values')}}</h2>
                    <p> <i>{{trans('content.about-values-desc')}}</i></p>
                    

                <a href="{{trans('header.menu-4-2-url')}}" class="button_all">{{trans('header.menu-4-2')}}</a>
                </div>
                <div class="col-md-5 col-sm-6 about_client">
                    {{-- <img src="/img/works-bg.jpeg" alt=""> --}}
                   <img src="/img/about-3.jpg" alt="Nosotros">
                </div>
            </div>
        </div>
    </section>
    <section id="{{trans('content.about-section-id-2')}}" class="call_min_area">
        <div class="container">
            <h2>+52 (624) 179 5885</h2>
            <p>{{trans('content.about-contact-desc')}}</p>
            <div class="call_btn">
                <a href="tel:+5216241795885" class="button_all">{{trans('content.about-btn-call')}}</a>
                <a href="mailto:cupulasjlf@gmail.com" class="button_all"><i class="fa fa-envelope-o"></i> {{trans('content.about-btn-quote')}}</a>
            </div>
        </div>
    </section>
    <!-- End About Us Area -->
