<!-- Header_Area -->
    <nav class="navbar navbar-default header_aera" id="main_navbar">
        <div class="container header-container">
            {{-- <!-- searchForm -->
            <div class="searchForm">
                <form action="#" class="row m0">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-search"></i></span>
                        <input type="search" name="search" class="form-control" placeholder="{{trans('header.search-ph')}}">
                        <span class="input-group-addon form_hide"><i class="fa fa-times"></i></span>
                    </div>
                </form>
            </div><!-- End searchForm --> --}}
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="col-md-3 col-xs-12 p0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#min_navbar">
                    <span class="sr-only">{{trans('header.menu')}}</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" style="z-index: 100;" href="{{trans('header.home-url')}}">
                        <img class="visible-md-block visible-lg-block visible-sm-block" src="img/logo3.png" style="width: 120px;margin-top: -15px;" alt="CABO BRICK DOMES">
                        <img class="visible-xs-block" src="img/logo2.png" style="width: 220px;margin-top: 0px;" alt="CABO BRICK DOMES">
                    </a>
                </div>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="col-md-9 col-xs-12 p0 margin-menu">
                <div class="collapse navbar-collapse" id="min_navbar">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown submenu">
                            <a href="{{trans('header.home-url')}}" class="dropdown-toggle" data-toggle="dropdown">{{trans('header.menu-1')}}</a>
                            <ul class="dropdown-menu">
                                <li><a href="{{trans('header.menu-1-1-url')}}">{{trans('header.menu-1-1')}}</a></li>
                                {{--  <li><a href="{{route('home')}}#testimoniales">Opiniones (Testimoniales)</a></li>  --}}
                            </ul>
                        </li>
                        <li class="dropdown submenu">
                            <a href="{{route('gallery')}}" class="dropdown-toggle" data-toggle="dropdown">{{trans('header.menu-2')}}</a>
                            <ul class="dropdown-menu other_dropdwn">
                                 <li><a href="{{trans('header.menu-2-1-url')}}">{{trans('header.menu-2-1')}}</a></li>
                                 <li><a href="{{trans('header.menu-2-2-url')}}">{{trans('header.menu-2-2')}}</a></li>
                                 <li><a href="{{trans('header.menu-2-3-url')}}">{{trans('header.menu-2-3')}}</a></li>
                                 <li><a href="{{trans('header.menu-2-4-url')}}">{{trans('header.menu-2-4')}}</a></li>
                                 <li><a href="{{trans('header.menu-2-5-url')}}">{{trans('header.menu-2-5')}}</a></li>
                                 {{-- <li><a href="{{trans('header.menu-2-6-url')}}">{{trans('header.menu-2-6')}}</a></li> --}}
                            </ul>
                        </li>
                        <li class="dropdown submenu">
                        <a href="{{route('works')}}" class="dropdown-toggle" data-toggle="dropdown">{{trans('header.menu-3')}}</a>
                            <ul class="dropdown-menu other_dropdwn">
                                <li><a href="{{trans('header.menu-3-1-url')}}">{{trans('header.menu-3-1')}}</a></li>
                                <li><a href="{{trans('header.menu-3-2-url')}}">{{trans('header.menu-3-2')}}</a></li>
                                <li><a href="{{trans('header.menu-3-3-url')}}">{{trans('header.menu-3-3')}}</a></li>
                                <li><a href="{{trans('header.menu-3-4-url')}}">{{trans('header.menu-3-4')}}</a></li>
                                <li><a href="{{trans('header.menu-3-5-url')}}">{{trans('header.menu-3-5')}}</a></li>
                                <li><a href="{{trans('header.menu-3-6-url')}}">{{trans('header.menu-3-6')}}</a></li>
                            </ul>
                        </li>
                        <li class="dropdown submenu">
                        <a href="/" class="dropdown-toggle" data-toggle="dropdown">{{trans('header.menu-5')}}</a>
                            <ul class="dropdown-menu other_dropdwn">
                                <li><a href="{{trans('header.menu-5-1-url')}}">{{trans('header.menu-5-1')}}</a></li>
                                <li><a href="{{trans('header.menu-5-2-url')}}">{{trans('header.menu-5-2')}}</a></li>
                                <li><a href="{{trans('header.menu-5-3-url')}}">{{trans('header.menu-5-3')}}</a></li>
                                <li><a href="{{trans('header.menu-5-4-url')}}">{{trans('header.menu-5-4')}}</a></li>
                                <li><a href="{{trans('header.menu-5-5-url')}}">{{trans('header.menu-5-5')}}</a></li>
                            </ul>
                        </li>
                        <li class="dropdown submenu">
                            <a href="{{route('about')}}" class="dropdown-toggle" data-toggle="dropdown">{{trans('header.menu-4')}}</a>
                            <ul class="dropdown-menu other_dropdwn">
                                <li><a href="{{trans('header.menu-4-1-url')}}">{{trans('header.menu-4-1')}}</a></li>
                                <li><a href="{{trans('header.menu-4-2-url')}}">{{trans('header.menu-4-2')}}</a></li>
                                <li><a href="{{trans('header.menu-4-3-url')}}">{{trans('header.menu-4-3')}}</a></li>
                                <li><a href="{{trans('header.menu-4-4-url')}}">{{trans('header.menu-4-4')}}</a></li>
                            </ul>
                        </li>

                        <li class="dropdown submenu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{trans('header.menu-6')}}</a>
                            <ul class="dropdown-menu other_dropdwn">
                                 <li><a href="tel:+526241795885"><i class="fa fa-phone"></i>{{trans('content.general',['es'=>'Oficina','en'=>'Office'])}} +52 (624) 179 5885</a></li>
                                <li><a href="tel:+526241272778"><i class="fa fa-phone"></i>{{trans('content.general',['es'=>'Móvil','en'=>'Mobile'])}} 1 +52 (624) 127 2778</a></li>
                                <li><a href="tel:+526241779118"><i class="fa fa-phone"></i>{{trans('content.general',['es'=>'Móvil','en'=>'Mobile'])}} 2 +52 (624) 177 9118</a></li>
                                <li><a href="mailto:cupulasjlf@gmail.com"><i class="fa fa-envelope-o"></i>cupulasjlf@gmail.com</a></li>
                            </ul>
                        </li>
                         <li class="dropdown submenu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-globe" aria-hidden="true"></i>&nbsp;{{trans('header.menu-7')}}</a>
                            <ul class="dropdown-menu other_dropdwn">
                                <li><a href="/">ESP.</a></li>
                                <li><a href="/en"> ENG.</a></li>
                            </ul>
                        </li>
                        {{--  <li><a href="contact.html">Contacto</a></li>  --}}
                        {{-- <li><a href="#" class="nav_searchFrom"><i class="fa fa-search"></i></a></li> --}}
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div>
        </div><!-- /.container -->
    </nav>
	<!-- End Header_Area -->