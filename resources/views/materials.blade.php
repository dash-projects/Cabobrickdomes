@extends('layouts.base')
@section('title',trans('header.menu-5'))
@section('header')
<style media="screen">
  /*
    font-family: 'Open Sans', sans-serif;
    font-family: 'Oswald', sans-serif;
    font-family: 'Raleway', sans-serif;
    font-family: 'Montserrat', sans-serif;
  */
  .text-os {
    font-family: 'Open Sans', sans-serif;
  }
  .text-od{
    font-family: 'Oswald', sans-serif;
  }
  .text-ry{
    font-family: 'Raleway', sans-serif;
  }
  .text-mt{
    font-family: 'Montserrat', sans-serif;
  }

</style>
<link  href="/vendors/fancybox-master/dist/jquery.fancybox.min.css" rel="stylesheet">
@endsection
@section('content')
   {{-- @include('theme2-section.work') --}}
   @include('theme2-section.materials')
@endsection
@section('scripts')
<!-- Animate JS -->
<script src="vendors/animate/wow.min.js"></script>
<!-- Stellar JS -->
<script src="vendors/stellar/jquery.stellar.min.js"></script>
<!-- Theme JS -->
<script src="js/theme.min.js"></script>
<script src="/vendors/fancybox-master/dist/jquery.fancybox.min.js"></script>
@endsection