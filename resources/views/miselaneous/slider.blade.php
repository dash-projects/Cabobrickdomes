<div class="herotextImage">
    <div class="container_full">
        <div class="slidershadow"> </div>

        <div class="mstslider">
            <!-- masterslider -->
            <div class="master-slider ms-skin-default" id="masterslider">
            <!-- slide -->
                <div class="ms-slide slide-1" data-delay="12" onclick="location.href='{{trans('content.slider1-url')}}';">

                    <!-- slide background -->
                    <img src="/img/blank.gif" data-src="/img/slider/01.jpg" alt="{{trans('content.slider1-title')}}"   data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"/>

                    <h2 class="ms-layer stext1" style="left: 130px; top: 262px;" data-type="text" data-duration="900" data-delay="1800" data-ease="easeOutExpo" data-effect="bottom(40)">
                        {!!trans('content.slider1-title')!!}<br>
                        {{trans('content.slider1-subtitle1')}}<br>
                        {{trans('content.slider1-subtitle2')}}
                    </h2>

                    <h4 class="ms-layer stext2" style="left: 130px; top: 380px;" data-type="text" data-duration="900" data-delay="2500" data-ease="easeOutExpo" data-effect="bottom(40)">
                        {{trans('content.slider1-btn')}}
                    </h4>
                </div>
            <!-- end of slide -->
            <!-- slide -->
                <div class="ms-slide slide-1" data-delay="12" onclick="location.href='{{trans('content.slider2-url')}}';">

                    <!-- slide background -->
                    <img src="/img/blank.gif" data-src="/img/slider/02.jpg" alt="{{trans('content.slider2-title')}}"   data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"/>

                    <h2 class="ms-layer stext1" style="left: 130px; top: 262px;" data-type="text" data-duration="900" data-delay="1800" data-ease="easeOutExpo" data-effect="bottom(40)">
                        {!!trans('content.slider2-title')!!}<br>
                        {{trans('content.slider2-subtitle1')}}<br>
                        {{trans('content.slider2-subtitle2')}}
                    </h2>

                    <h4 class="ms-layer stext2" style="left: 130px; top: 380px;" data-type="text" data-duration="900" data-delay="2500" data-ease="easeOutExpo" data-effect="bottom(40)">
                        {{trans('content.slider2-btn')}}
                    </h4>
                </div>
            <!-- end of slide -->
            <!-- slide -->
                <div class="ms-slide slide-1" data-delay="12" onclick="location.href='{{trans('content.slider3-url')}}';">

                    <!-- slide background -->
                    <img src="/img/blank.gif" data-src="/img/slider/03.jpg" alt="{{trans('content.slider3-title')}}"   data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"/>

                    <h2 class="ms-layer stext1" style="left: 130px; top: 262px;" data-type="text" data-duration="900" data-delay="1800" data-ease="easeOutExpo" data-effect="bottom(40)">
                        {!!trans('content.slider3-title')!!}<br>
                        {{trans('content.slider3-subtitle1')}}<br>
                        {{trans('content.slider3-subtitle2')}}
                    </h2>

                    <h4 class="ms-layer stext2" style="left: 130px; top: 380px;" data-type="text" data-duration="900" data-delay="2500" data-ease="easeOutExpo" data-effect="bottom(40)">
                        {{trans('content.slider3-btn')}}
                    </h4>
                </div>
            <!-- end of slide -->
            <!-- slide -->
                <div class="ms-slide slide-1" data-delay="12" onclick="location.href='{{trans('content.slider4-url')}}';">

                    <!-- slide background -->
                    <img src="/img/blank.gif" data-src="/img/slider/04.jpg" alt="{{trans('content.slider4-title')}}"   data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"/>

                    <h2 class="ms-layer stext1" style="left: 130px; top: 262px;" data-type="text" data-duration="900" data-delay="1800" data-ease="easeOutExpo" data-effect="bottom(40)">
                        {!!trans('content.slider4-title')!!}<br>
                        {{trans('content.slider4-subtitle1')}}<br>
                        {{trans('content.slider4-subtitle2')}}
                    </h2>

                    <h4 class="ms-layer stext2" style="left: 130px; top: 380px;" data-type="text" data-duration="900" data-delay="2500" data-ease="easeOutExpo" data-effect="bottom(40)">
                        {{trans('content.slider4-btn')}}
                    </h4>
                </div>
            <!-- end of slide -->
            <!-- slide -->
                <div class="ms-slide slide-1" data-delay="12" onclick="location.href='{{trans('content.slider5-url')}}';">

                    <!-- slide background -->
                    <img src="/img/blank.gif" data-src="/img/slider/05.jpg" alt="{{trans('content.slider5-title')}}"   data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"/>

                    <h2 class="ms-layer stext1" style="left: 130px; top: 262px;" data-type="text" data-duration="900" data-delay="1800" data-ease="easeOutExpo" data-effect="bottom(40)">
                        {!!trans('content.slider5-title')!!}<br>
                        {{trans('content.slider5-subtitle1')}}<br>
                        {{trans('content.slider5-subtitle2')}}
                    </h2>

                    <h4 class="ms-layer stext2" style="left: 130px; top: 380px;" data-type="text" data-duration="900" data-delay="2500" data-ease="easeOutExpo" data-effect="bottom(40)">
                        {{trans('content.slider5-btn')}}
                    </h4>
                </div>
            <!-- end of slide -->
            <!-- slide -->
                <div class="ms-slide slide-1" data-delay="12" onclick="location.href='{{trans('content.slider6-url')}}';">

                    <!-- slide background -->
                    <img src="/img/blank.gif" data-src="/img/slider/06.jpg" alt="{{trans('content.slider6-title')}}"   data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"/>

                    <h2 class="ms-layer stext1" style="left: 130px; top: 262px;" data-type="text" data-duration="900" data-delay="1800" data-ease="easeOutExpo" data-effect="bottom(40)">
                        {!!trans('content.slider6-title')!!}<br>
                        {{trans('content.slider6-subtitle1')}}<br>
                        {{trans('content.slider6-subtitle2')}}
                    </h2>

                    <h4 class="ms-layer stext2" style="left: 130px; top: 380px;" data-type="text" data-duration="900" data-delay="2500" data-ease="easeOutExpo" data-effect="bottom(40)">
                        {{trans('content.slider6-btn')}}
                    </h4>
                </div>
            <!-- end of slide -->
            
            </div>
            <!-- end of masterslider -->
        </div>
    </div>
</div>
@section('scripts')
@parent
<script src="/masterslider/masterslider.min.js" ></script>
<script type="text/javascript">
        (function($) {
            "use strict";
            var slider = new MasterSlider();
            slider.setup('masterslider' , {
                width: 1400,    // slider standard width
                height:580,   // slider standard height
                space:0,
                speed:45,
                fullwidth:true,
                loop:true,
                preload:0,
                autoplay:true,
                view:"basic"
            });
// adds Arrows navigation control to the slider.
            slider.control('arrows');
            slider.control('bullets');
        })(jQuery);
    </script>
@endsection