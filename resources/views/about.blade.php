@extends('layouts.base')
@section('title',trans('header.menu-4'))
@section('header')
<style media="screen">
  /*
    font-family: 'Open Sans', sans-serif;
    font-family: 'Oswald', sans-serif;
    font-family: 'Raleway', sans-serif;
    font-family: 'Montserrat', sans-serif;
  */
  .text-os {
    font-family: 'Open Sans', sans-serif;
  }
  .text-od{
    font-family: 'Oswald', sans-serif;
  }
  .text-ry{
    font-family: 'Raleway', sans-serif;
  }
  .text-mt{
    font-family: 'Montserrat', sans-serif;
  }

</style>
@endsection
@section('content')
   @include('theme2-section.about')
   @include('theme2-section.features')
  @include('theme2-section.archievments')
@endsection
@section('scripts')
<!-- Animate JS -->
<script src="vendors/animate/wow.min.js"></script>
<!-- Stellar JS -->
<script src="vendors/stellar/jquery.stellar.min.js"></script>
<!-- Progress JS -->
<script src="vendors/Counter-Up/jquery.counterup.min.js"></script>
<script src="vendors/Counter-Up/waypoints.min.js"></script>
<!-- Theme JS -->
<script src="js/theme.js"></script>
<script src="/vendors/fancybox-master/dist/jquery.fancybox.min.js"></script>
@endsection
