@extends('layouts.base')
@section('title',trans('header.menu-2'))
@section('header')
<style media="screen">
  /*
    font-family: 'Open Sans', sans-serif;
    font-family: 'Oswald', sans-serif;
    font-family: 'Raleway', sans-serif;
    font-family: 'Montserrat', sans-serif;
  */
  .text-os {
    font-family: 'Open Sans', sans-serif;
  }
  .text-od{
    font-family: 'Oswald', sans-serif;
  }
  .text-ry{
    font-family: 'Raleway', sans-serif;
  }
  .text-mt{
    font-family: 'Montserrat', sans-serif;
  }
  
  @media (min-width: 768px) { 
    .carousel-inner > .item > img.slider{
      margin:auto;
      max-height:550px;
      max-width:100%;
    }
  }
  @media (max-width: 767px) {
    .carousel-inner > .item > img.slider{
      margin:auto;
      height:200px;
      max-width:100%;
    }
  }
 .carousel-indicators li {
   display: none;
 }
</style>
@endsection
@section('content')
  {{-- <center style="margin-top:200px;margin-bottom:200px;S"><h1>GALERIA PROXIMAMENTE</h1></center> --}}
     @include('theme2-section.slide') 
@endsection
@section('scripts')
<!-- Animate JS -->
<script src="vendors/animate/wow.min.js"></script>
<!-- Stellar JS -->
<script src="vendors/stellar/jquery.stellar.min.js"></script>
<!-- Theme JS -->
<script src="js/theme.min.js"></script>
<script src="/vendors/fancybox-master/dist/jquery.fancybox.min.js"></script>
@endsection