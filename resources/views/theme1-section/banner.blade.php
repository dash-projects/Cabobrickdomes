<!-- start banner Area -->
<section class="banner-area relative" id="home" data-parallax="scroll" data-image-src="img/bg4.jpeg">
  <div class="overlay-bg overlay"></div>
  <div class="container">
    <div class="row fullscreen d-flex align-items-center justify-content-center">
      <div class="banner-content col-lg-10 col-md-12">
        <h4 class="text-white">Conocemos mejor que nadie</h4>
        <h1 class="text-white text-os">
          El Arte de hacer domos de <span style="color:#f26522;">ladrillo</span>
        </h1>
        <p class="text-white">
          Hornos, Domos, Chimeneas y acabados de ladrillo
        </p>
        <a href="{{route('galery')}}" class="primary-btn header-btn text-uppercase">Conoce m&aacute;s</a>
      </div>
    </div>
  </div>
</section>
<!-- End banner Area -->
