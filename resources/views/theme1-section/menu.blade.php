<header id="header" id="home">
  <div class="container">
    <div class="row align-items-center justify-content-between d-flex">
      <div id="logo">
        <a href="index.html"><img src="img/logo.png" alt="" title="" /></a>
      </div>
      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="{{route('home')}}">Inicio</a></li>
          <li><a href="{{route('galery')}}">Galeria</a></li>
          <li class="menu-has-children"><a href="">Trabajos</a>
            <ul>
              <li><a href="{{route('domes')}}">Domos</a></li>
              <li><a href="{{route('oven')}}">Hornos</a></li>
              <li><a href="{{route('fireplace')}}">Chimeneas</a></li>
            </ul>
          </li>
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </div>
</header><!-- #header -->
