@extends('layouts.app')
@section('title','Inicio')
@section('header')
<style media="screen">
  /*
    font-family: 'Open Sans', sans-serif;
    font-family: 'Oswald', sans-serif;
    font-family: 'Raleway', sans-serif;
    font-family: 'Montserrat', sans-serif;
  */
  .text-os {
    font-family: 'Open Sans', sans-serif;
  }
  .text-od{
    font-family: 'Oswald', sans-serif;
  }
  .text-ry{
    font-family: 'Raleway', sans-serif;
  }
  .text-mt{
    font-family: 'Montserrat', sans-serif;
  }

</style>
@endsection
@section('content')
@include('section.menu')

@include('section.banner')

@include('section.contact')

@include('section.footer')
@endsection
@section('script')
<script type="text/javascript">
var form;
var submit;
var alert;
// -------   Mail Send ajax
function sendContact(){
  $.ajax({
      url: "{{route('send-contact')}}", // form action url
      type: "post", // form submit method get/post
      data: form.serialize(), // serialize form data
      beforeSend: function() {
          alert.fadeOut();
          submit.html('Enviando....'); // change submit button text
      },
      success: function(data) {
        console.log(data);
          //
          // alert.html(data).fadeIn(); // fade in response data
          // form.trigger('reset'); // reset form
          // submit.attr("style", "display: none !important");; // reset submit button text
      },
      error: function(e) {
          console.log(e)
      }
  });
}

 $(document).ready(function() {
     form= $('#myForm'); // contact form
     submit= $('#sendContact'); // submit button
     alert= $('.alert-msg'); // alert div for show alert message

    // form submit event
    submit.on('click',sendContact);
});

</script>
@endsection
