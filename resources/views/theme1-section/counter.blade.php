<!-- Start fact Area -->
<section class="facts-area section-gap">
  <div class="container">
    <div class="row">
      <div class="col single-fact">
        <h1 class="counter">2536</h1>
        <p>Projects Completed</p>
      </div>
      <div class="col single-fact">
        <h1 class="counter">6784</h1>
        <p>Really Happy Clients</p>
      </div>
      <div class="col single-fact">
        <h1 class="counter">1059</h1>
        <p>Total Tasks Completed</p>
      </div>
      <div class="col single-fact">
        <h1 class="counter">2239</h1>
        <p>Cups of Coffee Taken</p>
      </div>
      <div class="col single-fact">
        <h1 class="counter">435</h1>
        <p>In House Professionals</p>
      </div>
    </div>
  </div>
</section>
<!-- end fact Area -->
