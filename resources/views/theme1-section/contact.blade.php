<!-- start contact Area -->
<section class="contact-area section-gap" id="contact">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="menu-content pb-30 col-lg-8">
        <div class="title text-center">
          <h1 class="mb-10">¿Quieres algo en especial?<br>Cont&aacute;ctanos</h1>
        </div>
      </div>
    </div>
    <form class="form-area mt-60" id="myForm" class="contact-form text-right">
      <div class="row">
      <div class="col-lg-6 form-group">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input name="name" placeholder="introduce tu nombre" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Introduce tu nombre'" class="common-input mb-20 form-control" required="" type="text">

        <input name="email" placeholder="Introduce tu correo electrónico" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Introduce tu correo electrónico'" class="common-input mb-20 form-control" required="" type="email">

        <input name="subject" placeholder="Introduzca el asunto del correo" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Introduzca el asunto del correo'" class="common-input mb-20 form-control" required="" type="text">
      </div>
      <div class="col-lg-6 form-group">
        <textarea class="common-textarea mt-10 form-control" name="message" placeholder="Mensaje" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Mensaje'" required=""></textarea>
        <a href="#" id="sendContact" class="primary-btn mt-20">Enviar Mensaje<span class="lnr lnr-arrow-right"></span></a>
        <div class="mt-10 alert-msg">
      </div>
      </div></div>
    </form>

  </div>
</section>
<!-- end contact Area -->
