<!-- start footer Area -->
<footer class="footer-area section-gap">
  <div class="container">
    <div class="row">
      <div class="col-lg-3  col-md-6 col-sm-6">
        <div class="single-footer-widget">
          <h4 class="text-white">Acerca de nosotros</h4>
          <p>
            Somos una empresa familiar que aprendimos el arte de trabajar el ladrillo para realizar diferentes trabajos, conoce nuestro trabajo y dejate cautivar por la belleza del ladrillo.
          </p>
        </div>
      </div>
      <div class="col-lg-4  col-md-6 col-sm-6">
        <div class="single-footer-widget">
          <h4 class="text-white">Contáctanos</h4>
          <p>
          Puedes contactarnos a traves de los siguientes medios
          </p>
          <p class="number">
            052-624-000-0000 <br>
            052-624-000-0000
          </p>
        </div>
      </div>
      <div class="col-lg-5  col-md-6 col-sm-6">
        <div class="single-footer-widget">
          <h4 class="text-white">¿Quieres recibir promociones?</h4>
          <p>Puedes confiar en nosotros, te enviarémos ofertas, no spam</p>
          <div class="d-flex flex-row" id="mc_embed_signup">


              <form id='nwForm' class="navbar-form" novalidate="true">
                <div class="input-group add-on">
                    <input class="form-control" name="nw-mail" id="nw-mail" placeholder="Correo Electrónico" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Correo Electrónico'" required="" type="email">
                <div style="position: absolute; left: -5000px;">
                  <!-- <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text"> -->
                </div>
                  <div class="input-group-btn">
                    <a href="#" onclick="subscribe();" class="genric-btn primary circle arrow"><span class="lnr lnr-arrow-right"></span></a>
                  </div>
                </div>
                  <div class="info mt-20"></div>
              </form>

          </div>
        </div>
      </div>
    </div>
    <div class="footer-bottom d-flex justify-content-between align-items-center flex-wrap">

      <p class="footer-text m-0">Copyright &copy;<script>document.write(new Date().getFullYear());</script> Todos los derechos reservados | Desarrollado por <a href="http://cabodash.com">cabodash</a> </p>

      <div class="footer-social d-flex align-items-center">
        <a href="https://fb.me/cabobrickdomes"><i class="fa fa-facebook"></i></a>
        <!-- <a href="#"><i class="fa fa-twitter"></i></a>
        <a href="#"><i class="fa fa-dribbble"></i></a>
        <a href="#"><i class="fa fa-behance"></i></a> -->
      </div>
    </div>
  </div>
</footer>
<!-- End footer Area -->
