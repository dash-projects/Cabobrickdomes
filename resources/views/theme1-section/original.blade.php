@extends('layouts.app')
@section('title','Inicio')
@section('header')
<style media="screen">
  /*
    font-family: 'Open Sans', sans-serif;
    font-family: 'Oswald', sans-serif;
    font-family: 'Raleway', sans-serif;
    font-family: 'Montserrat', sans-serif;
  */
  .text-os {
    font-family: 'Open Sans', sans-serif;
  }
  .text-od{
    font-family: 'Oswald', sans-serif;
  }
  .text-ry{
    font-family: 'Raleway', sans-serif;
  }
  .text-mt{
    font-family: 'Montserrat', sans-serif;
  }

</style>
@endsection
@section('content')
@include('section.menu')

@include('section.banner')

@include('section.feature')

@include('section.offered')

@include('section.projects')

@include('section.counter')

@include('section.services')

@include('section.testimonial')

@include('section.blog')

@include('section.branding')

@include('section.contact')

@include('section.footer')
@endsection
