<!-- Start offered Area -->
<section class="offered-area section-gap" id="offered">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="menu-content pb-60 col-lg-8">
        <div class="title text-center">
          <h1 class="mb-10">Some Features that Made us Unique</h1>
          <p>Who are in extremely love with eco friendly system.</p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-4">
        <div class="single-offered">
          <img class="img-fluid" src="img/s1.png" alt="">
          <a href="#"><h4 class="pt-20 pb-20">Basic & Common Repairs</h4></a>
          <p>
            Computer users and programmers have become so accustomed to using Windows, even for the changing capabilities and the appearances of the graphical.
          </p>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="single-offered">
          <img class="img-fluid" src="img/s2.png" alt="">
          <a href="#"><h4 class="pt-20 pb-20">Brake Repairs & Services</h4></a>
          <p>
            Computer users and programmers have become so accustomed to using Windows, even for the changing capabilities and the appearances of the graphical.
          </p>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="single-offered">
          <img class="img-fluid" src="img/s3.png" alt="">
          <a href="#"><h4 class="pt-20 pb-20">Preventive Maintenance</h4></a>
          <p>
            Computer users and programmers have become so accustomed to using Windows, even for the changing capabilities and the appearances of the graphical.
          </p>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- End offered Area -->
