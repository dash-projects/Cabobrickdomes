<!-- Start features Area -->
<section class="features-area pt-100" id="feature">
  <div class="container"><div class="feature-section">
    <div class="row">
      <div class="single-feature col-lg-4">
        <img src="img/f1.png" alt="">
        <h4 class="pt-20 pb-20">Building Drawings</h4>
        <p>
          Lorem ipsum dolor sit amet, consecteturadipis icing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        </p>
      </div>
      <div class="single-feature col-lg-4">
        <img src="img/f2.png" alt="">
        <h4 class="pt-20 pb-20">Building Drawings</h4>
        <p>
          Lorem ipsum dolor sit amet, consecteturadipis icing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        </p>
      </div>
      <div class="single-feature col-lg-4">
        <img src="img/f3.png" alt="">
        <h4 class="pt-20 pb-20">Building Drawings</h4>
        <p>
          Lorem ipsum dolor sit amet, consecteturadipis icing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        </p>
      </div>
      </div>
    </div>
  </div>
</section>
<!-- End features Area -->
