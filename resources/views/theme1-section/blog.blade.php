<!-- Start latest-blog Area -->
<section class="latest-blog-area section-gap" id="blog">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="menu-content pb-60 col-lg-8">
        <div class="title text-center">
          <h1 class="mb-10">Latest From Blog</h1>
          <p>Who are in extremely love with eco friendly system.</p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="blog-left col-lg-4 col-md-6">
        <div class="single-blog">
          <img class="mx-auto d-block img-fluid" src="img/b1.jpg" alt="">
          <div class="desc">
             <a href="#"><h5>We do Believe in Quality</h5></a>
             <p>
              inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct standards.
             </p>
             <a class="read-more text-uppercase" href="#">Read More</a>
          </div>
        </div>
      </div>
      <div class="blog-left col-lg-4 col-md-6">
        <div class="single-blog">
          <img class="mx-auto d-block img-fluid" src="img/b2.jpg" alt="">
          <div class="desc">
             <a href="#"><h5>We do Believe in Quality</h5></a>
             <p>
              inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct standards.
             </p>
             <a class="read-more text-uppercase" href="#">Read More</a>
          </div>
        </div>
      </div>
      <div class="blog-right col-lg-4 col-md-6">
        <div class="single-list justify-content-start d-flex">
          <div class="thumb">
            <img src="img/b3.jpg" alt="">
          </div>
          <div class="details">
            <a href="#"><h6>We Believe in Quality</h6></a>
            <p>21st Dec  04 Comments</p>
          </div>
        </div>
        <div class="single-list justify-content-start d-flex">
          <div class="thumb">
            <img src="img/b4.jpg" alt="">
          </div>
          <div class="details">
            <a href="#"><h6>We Believe in Quality</h6></a>
            <p>21st Dec  04 Comments</p>
          </div>
        </div>
        <div class="single-list justify-content-start d-flex">
          <div class="thumb">
            <img src="img/b5.jpg" alt="">
          </div>
          <div class="details">
            <a href="#"><h6>We Believe in Quality</h6></a>
            <p>21st Dec  04 Comments</p>
          </div>
        </div>
        <div class="single-list justify-content-start d-flex">
          <div class="thumb">
            <img src="img/b6.jpg" alt="">
          </div>
          <div class="details">
            <a href="#"><h6>We Believe in Quality</h6></a>
            <p>21st Dec  04 Comments</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- End latest-blog Area -->
